from classification import Classification, Classification_group
from utils.xml_utils import set_classification_group_to_XML, create_output
from configparser import ConfigParser
import os

class Controller():
    """Controller of the MVC architecture of the classification tool
    This class must be used as a singleton to manage the whole process of adding a new classification system
    In fact, even trying to create multiple instances of this class, only one will be created, all the others will be alias

    Public attribute:
    config -- parsed config.ini file (linked by "../config.ini")

    Public methodes:
    create_new_classification_group -- create a classification system, and set it as the current classification system
    create_new_classification -- create a classification value and append it to the current classification system
    save_all_group_to_XML -- save the current configuration of the new classification system to the XML file in output
    print_group -- print the current classification system into the standard output
    clean_group -- remove the current classification system and set it to None
    update_lang -- change the language of the current classification system
    """

    _instance = None

    def __new__(cls):
        """Constructor of a singleton for the Controller class"""
        if cls._instance is None:
            cls._instance = object.__new__(cls)
            cls._NEW_CLASSIFICATION_GROUP = None
            cls._LANG = "en"
            cls.config = ConfigParser()
            cls.config.read(os.path.join(os.path.dirname(__file__), "../config.ini"))
        return cls._instance

    def create_new_classification_group(self, name, type_classification):
        """Create a new classification system and set this one to current
        
        Keyword arguments:
        name -- the name of the new classification system
        type_classification -- the classification system type (1, 2 or 3, see ecospoild2 documentation)
        
        Returns the uuid of the new classification system
        """

        self._NEW_CLASSIFICATION_GROUP = Classification_group(name, type_classification)

        return self._NEW_CLASSIFICATION_GROUP.get_uuid()

    def create_new_classification(self, name, comment=None):
        """Create a new classification value and append it to the current classification system of the controller

        Keyword arguments:
        name -- the name of the new value in the classification system
        comment -- comment to present the new value in the classification system

        Raise:
        RuntimeError -- if the current classification group of the controller isn't setup

        Returns the uuid of the new classification value
        """ 

        if not self._NEW_CLASSIFICATION_GROUP:
            raise RuntimeError("No classification group has been previously created, please call Controller.create_new_classification_group before calling Controller.create_new_classification")
        
        new_class = Classification(name, comment)
        self._NEW_CLASSIFICATION_GROUP.set_value(new_class)

        return new_class.get_uuid()

    def save_all_group_to_XML(self, overwrite=False):
        """Save the current configuration of the new classification system to the XML output file
        Warning: This operation will overwrite any previous output file, and the output isn't reload in the controller as an input, so you can't append multiple new classification system in one runtime of the program using the controller

        Keyword argument:
        overwrite -- if set to True, the output path will be overwrite by the one of the input. Be careful, this will make no copy of the input file before overwrite it (default False)
        """

        tree = set_classification_group_to_XML(self._NEW_CLASSIFICATION_GROUP, self.config["PATHS"]["CLASSIFICATIONS_PATH"])

        if overwrite:
            create_output(tree, path=self.config["PATHS"]["CLASSIFICATIONS_PATH"])
        else:
            create_output(tree, path=self.config["PATHS"]["OUTPUT_PATH"])

        self._NEW_CLASSIFICATION_GROUP = None
        
    def print_group(self):
        """Print the current classification system in the standard output"""
        if self._NEW_CLASSIFICATION_GROUP:
            print("Classification system: ")
            self._NEW_CLASSIFICATION_GROUP.show()
        else:
            print("No current classification system setup...")

    def clean_group(self):
        """Remove current classification system of the controller"""
        self._NEW_CLASSIFICATION_GROUP = None
    
    def update_lang(code):
        """Set the language of the current classification system"""
        self._LANG = code
