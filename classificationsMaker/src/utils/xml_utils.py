import xml.etree.ElementTree as ET

def set_classification_group_to_XML(cl_group, file_path):
    tree = ET.parse(file_path)
    cl_group.to_XML_element(tree)
    return tree

def create_output(tree, path, xmlns=""):
    if not xmlns and tree.getroot().tag[0] == '{':
        xmlns = tree.getroot().tag[1:].partition("}")[0]
    ET.register_namespace("", xmlns)
    ET.indent(tree, space="\t", level=0)
    tree.write(path, encoding="utf-8", xml_declaration=True)
