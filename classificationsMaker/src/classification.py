from uuid import uuid4
import xml.etree.ElementTree as ET

class Classification():
    """Represents a value inside a classification system

    Attributes:
    uuid -- auto-generated uuid of the value
    name -- the name of the value (non empty string)
    lang -- language of the name and the comment of the value (2-letters code, default "en")
    comment -- comment to explain the value (string)

    Methodes:
    get_uuid -- getter of value's uuid
    get_lang -- getter of value's language
    to_XML_element -- turn the value to a branch in an XML tree (parent given)
    show -- print the value parameters in the standard output
    """

    def __init__(self, name, comment=None, lang="en"):
        """Constructor of the Classification class
        
        Keyword arguments:
        name -- the name of the value represented by the instance
        comment -- the presentation of the value represented by the instance (default None)
        lang -- the two-letters language code (default "en")

        Raise:
        ValueError -- if the name string is empty
        ValueError -- if the lang string isn't a two-letters string
        """

        if not name:
            raise ValueError("Empty name for classification value")
        
        if len(lang) != 2:
            raise ValueError("Lang should be a two-letters string")
        
        self.uuid = uuid4()
        self.name = name
        self.lang = lang
        self.comment = comment
    
    def get_uuid(self):
        """Getter of the Classification's uuid"""
        return self.uuid
    
    def get_lang(self):
        """Getter of the Classification's language"""
        return self.lang
    
    def to_XML_element(self, parent):
        """Transform Classification to its XML equivalent, and append it to the XML tree

        Keyword argument:
        parent -- XML tree element (from xml.etree.ElementTree), which represents the parent's node of the Classification in the XML tree

        Returns the XML element generated, but this one as already been append to the XML tree thanks to the "parent" argument
        """

        self_XML_element = ET.SubElement(parent, "classificationValue", attrib={"id": str(self.get_uuid())})
        
        self_XML_name = ET.SubElement(self_XML_element, "name", attrib={"xml:lang": self.get_lang()})
        self_XML_name.text = self.name

        if self.comment:
            self_XML_comment = ET.SubElement(self_XML_element, "comment", attrib={"xml:lang": self.get_lang()})
            self_XML_comment.text = self.comment
        
        return self_XML_element
    
    def show(self, tab=""):
        """Print the Classification's instance in the standard output"""
        print(tab + "UUID:", self.uuid)
        print(tab + "Name:", self.name)
        if self.comment:
            print(tab + "Comment:", self.comment)


class Classification_group():
    """Represents a system of classification (set of values)

    Attributes:
    uuid -- auto-generated uuid of the value
    name -- the name of the classification system (non empty string)
    lang -- language of the name and the comment of the classification system (2-letters code, default "en")
    type -- type of classification system (1 = activity classification, 2 = product classification, 3 = activity and product classification)
    values -- set of Classification instances, which represents all possible values of the classification system
    
    Methods:
    set_value -- append a Classification instance to the set of values of the classification system
    get_uuid -- getter of the uuid of the classification system
    get_type -- getter of the type of the classification system
    get_lang -- getter of the language of the classification system
    to_XML_element -- transform the classification system to an XML element and append it to the XML tree as a subelement of the root
    show -- print the classification system in the standard output
    """

    def __init__(self, name, type_classification, lang="en"):
        """Constructor of Classification_group class

        Keyword arguments:
        name -- the name of the classification system
        type_classification -- the type of classification (1 = activity classification, 2 = product classification, 3 = activity and product classification), see ecospoild2 documentation for more details
        lang -- language (two letters code) of name and comment of the classification system (défault "en")
        
        Raise:
        ValueError -- if the name string is empty
        ValueError -- if the lang string isn't a two-letters string
        ValueError -- if the type of classification isn't valid
        """

        if not name:
            raise ValueError("Empty name for classification system")
        
        if len(lang) != 2:
            raise ValueError("Lang should be a two-letters string")
        
        if str(type_classification) not in ("1", "2", "3"):
            raise ValueError("type_classification should be 1, 2 or 3")

        self.uuid = uuid4()
        self.name = name 
        self.lang = lang
        self.type = type_classification
        self.values = []

    def set_value(self, value):
        """Append a value to the classification system. The value should be an instance of the Classification class"""
        self.values.append(value)

    def get_uuid(self):
        """Getter of the uuid of the classification system"""
        return self.uuid
    
    def get_type(self):
        """Getter of the type of the classification system"""
        return self.type
    
    def get_lang(self):
        """Getter of the language of the classification system"""
        return self.lang

    def to_XML_element(self, tree):
        """Transform the classification system represented by the instance of Classification_group to an XML element and append it to the tree with the tree root as parent
        
        Keyword argument:
        tree -- XML tree of the Classifications.xml official file
        """

        self_XML_element = ET.SubElement(tree.getroot(), "classificationSystem", attrib={"id": str(self.get_uuid()), "type": str(self.get_type())})
        self_XML_name = ET.SubElement(self_XML_element, "name", attrib={"xml:lang": self.get_lang()})
        self_XML_name.text = self.name
        for val in self.values:
            val.to_XML_element(self_XML_element)

        return self_XML_element

    def show(self, tab=""):
        """Print Classification_group instance in the standard output"""
        print(tab + "UUID:", self.uuid)
        print(tab + "Name:", self.name)
        print(tab + "Type:", self.type)
        for i, val in enumerate(self.values):
            print("Classification value number", i + 1, ":")
            val.show(tab = tab + "\t")


