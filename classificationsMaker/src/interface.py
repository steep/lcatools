from controller import Controller

def terminal_interface():
    """Basic terminal interface to create a new classification system"""
    global controller
    try:
        controller = Controller()
    except FileNotFoundError:
        print("config.ini not found at '../config.ini'")
        exit(1)
    next_group = True

    print("Hello!")
    while next_group:
        _input_classification_group()
        _set_classifications_values()
        print("----------")
        controller.print_group()
        if _validate("Validate new set of classification?"):
            try:
                controller.save_all_group_to_XML()
            except FileNotFoundError:
                print("Impossible to find input or output file... Aborting")
                exit(1)
            except KeyError:
                print("config.ini global path variable name not valid... Aborting")
                exit(1)

            print("Done")
            next_group = False
        else:
            next_group = _validate("Setup another group of categories?")
    print("See you!")

def _input_classification_group():
    """Terminal interface: private function to ask for the creation of a new classification system"""
    name = ""
    while not name:
        name = input("Group name: ")

    autorized_values_type = ('1', '2', '3')
    class_type = input("Group type (h for help): ")
    while class_type not in autorized_values_type:
        if class_type.upper() != 'H':
            print("Not a valid input")
        print("Group type is a number between 1 and 3: \n 1 = activity classification\n 2 = product classification\n 3 = activity and product classification")
        class_type = input("Group type (h for help): ")
    
    uuid = controller.create_new_classification_group(name, class_type)

    print("UUID of new classification group: ", uuid)

def _set_classifications_values():
    """Terminal interface: private function to append new classification value to classification system"""
    next_value = True
    cnt = 1
    while next_value:
        name = ""
        while name == "":
            name = input("Classification value number " + str(cnt) + " name: ")
        
        comment = input("Classification value number " + str(cnt) + " comment (enter to skipe): ")

        uuid = controller.create_new_classification(name, comment)
        print("UUID of new classification value:", uuid)

        cnt += 1

        next_value = _validate("Add another classification value to group?")
        
def _validate(message):
    """Terminal interface: private function to be sure that a yes or a no is explicitly given by the user"""
    cmd = input(message + " (y/n): ")
    while cmd.upper() not in ('Y', 'N', 'YES', 'NO'):
        print("Unknown command \"" + cmd + "\"")
        cmd = input(message + " (y/n): ")
    if cmd.upper() in ("Y", "YES"):
        return True 
    return False
 
