#!/usr/bin/env python3

from interface import terminal_interface

def main():
    """Begin here the interface"""
    terminal_interface()
    
if __name__ == "__main__":
    main()
