# Classification group maker tool

**Author: Baptiste de GOËR**

**Team: STEEP**

## Problematic

The [Ecospold2](https://ecoinvent.org/the-ecoinvent-database/data-formats/ecospold2/) data-format of LCA Ecoinvent database allows categorisation of activities and products.

Every categorisation is a tuple of two values: the classification system (or classification group) and the classification value inside the system.

Thoses classification systems and the associated values are referenced in the `MasterData/Classifications.xml` file of the ecoinvent database.

The goal of this tool is to provide a way to append a classification system to this XML file.

## How to use

1. Configure the `config.ini` file with the path to the already-existing `Classifications.xml` Ecoinvent file (usualy in `ecoinvent/MasterData/Classifications.xml`). You can also configure the path to the output file.
2. **Just run the `src/main.py` script**

### Librairies

Default implemented librairies should be enough to run the program, if not, please install `xml`, `configparser`, `os` and `uuid` python3 lib:

### Code

The code is structured as an [MVC](https://en.wikipedia.org/wiki/Model%E2%80%93view%E2%80%93controller) architecture:

* `classification.py`: Model
    * classification.Classification
    * classification.Classification_group
* `controller.py`: Controller (to use as a [singleton](https://en.wikipedia.org/wiki/Singleton_pattern))
    * controller.Controller
* `interface.py`: View

```mermaid
classDiagram
    direction LR
    class Interface
    class Controller {
        <<Singleton>>
        -String _LANG
        +create_new_classification_group()
        +create_new_classification()
        +save_all_group_to_XML()
        +print_group()
        +clean_group()
        +update_lang()
    }
    class Classification_group {
        +uuid4 uuid
        +String name
        +String lang
        +String type
        +set_value()
        +to_XML_element()
        +show()
    }
    class Classification {
        +uuid4 uuid
        +String name
        +String lang
        +String comment
        +to_XML_element()
        +show()
    }
    Interface --> Controller : controller 
    Controller --> Classification_group : _NEW_CLASSIFICATION_GROUP
    Classification_group --o Classification : values
```

### Use backend to create a new interface
If you want to develop your own interface, you can use the `controller.Controller` class:
1. Create a new instance of `Controller` (the only one, as a singleton)
2. Call `Controller.create_new_classification_group(name, type)` to create a new system of classification
3. (Optionnal, can be called at any time) Change the language of the new system by calling `Controller.update_lang(code)`: the `code` is the two-letters language code
4. Call `Controller.create_new_classification(name, comment=None)` to create a new classification value into the classification system
5. Call `Controller.save_all_group_to_XML()` to create the `output.xml` containing the old and the new classification systems

You can print in the terminal the current classification system by calling `Controller.print_group()`

You can destroy the current new classification system to reset the controller by calling `Controller.clean_group()`

## Features

* Setup a new classification system, including name and type
* Add as many classification values as you like to the new classification system, including name and comment fields
* Configure input and output path in configuration file

### Not yet implemented

* Selection of the name and comment language (english by default)
* The possibility to add multiple classification systems at the same time
* The possibility to see the classification systems which are yet implemented and to check if a name is already used
* The possibility to modify or remove an implemented classification system
* Graphical interface
