#!/usr/bin/env python3

import brightway2 as bw2 
import argparse as ap
import os
from configparser import ConfigParser

def setup(force=False):
    """Create environnement needed to use the isTerreTool app, 
    including importing ecoinvent database.
    
    Exit program if 'config.ini' isn't setup.
    
    Args:
        force (bool): If set to True, reinitialize the all setup, 
            even if it as already been done.
    """

    print("Setup begin")

    # Get configuration
    config = ConfigParser()
    config.read(os.path.join(os.path.dirname(__file__), "config.ini"))
    try:
        ecoinvent_path = config["SETUP"]["ECOINVENT"]
    except KeyError:
        print("'config.ini' isn't valid, you must have the ecoinvent's datasets path set up into the 'ECOINVENT' variable into the 'SETUP' section...")
        exit(1)
    if not os.path.exists(ecoinvent_path):
        print("Please configure ecoinvent path in 'config.ini', SETUP section...")
        exit(1)

    # Setup bw2
    project_name = config["SETUP"]["PROJECT_NAME"]
    if force:
        try:
            print("Forcing setup...")
            bw2.projects.delete_project(project_name, delete_dir=True)
        except ValueError:
            print("No force needed, project not previously setup")
    bw2.projects.set_current(project_name)
    bw2.bw2setup()

    # Import ecoinvent
    if "ecoinvent" not in bw2.databases:
        importer = bw2.SingleOutputEcospold2Importer(ecoinvent_path, "ecoinvent")
        importer.apply_strategies()
        if importer.statistics()[2]:
            print("Some unlinked exchanges has been detected, impossible to init ecoinvent database...")
            exit(1)
        importer.write_database()
    else:
        print("Ecoinvent is already imported.")

    # Create output database
    new_database_name = config["SETUP"]["DATABASE_NAME"]
    if new_database_name not in bw2.databases:
        out = bw2.Database(new_database_name)
        out.write({})
    
    print("Setup finished")

def setup_parser():
    """Initilize and parse parser of command-line argument

    Command line arguments available with this function:

    * -f: If initialization has already been done with
        the current configuration setup, it will force 
        reinitialization.

    Returns:
        ArgumentParser: Parsed argparse.ArgumentParser instance.
    """
    
    parser = ap.ArgumentParser()
    parser.add_argument('-f', '--force', help='Force setup initialization', action='store_true')
    args = parser.parse_args()
    return args

def main():
    """Entry point for setup script.
    """

    args = setup_parser()
    setup(force=args.force)

if __name__ == "__main__":
    main()
