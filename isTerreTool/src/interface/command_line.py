from ..controller.controller import Controller
from ..API.data import export_biosphere_flows
from ..utils.tests import STATS_NAMES, save_and_print_testing_result
from ..utils.dataobj import check_config

from termcolor import colored

def command_line_interface(export_bio=False, export_tech=False, test=False):
    """Offer a command-line interface to create a group of Brightway2 Activities
    and export it at the end. The interactions are in english.

    Args:
        export_bio (bool, optional): If set to True, after the groupment,
            the biosphere flows of the grouped activity are exported
            en the "output" folder, as a csv file. Defaults to False.
        export_tech (bool, optional): If set to True, after the groupment,
            the technosphere flows of the grouped activity are exported
            en the "output" folder, as a csv file. Defaults to False.
        test (bool, optional): If set to True (which is highly recommended),
            the grouped data will be tested to be sure that it is reliable.
            Defaults to False.
    """

    ctrl = Controller()

    print("Glad to see you!")
    name = ''
    while name == '':
        name = input("Please name the groupment you wish to do: ")

    info = dict()

    get_some_info(info, "location")
    if get_some_info(info, "reference product"):
        info['type'] = "process"
        print("You have given a reference product, you can give it an unit and an amount.")
        get_some_info(info, "unit")
        get_some_info(info, "production amount")
    get_some_info(info, "comment")
    
    cont = True
    while cont:
        if not ctrl.actvity_list:
            choice = get_begin_act(ctrl, to_show=["reference product"])
            ctrl.validate_first_activity(name, choice, info)
        
        act, _ = ctrl.find_linked_activities()
        print("Set of linked activities (enter to stop): ")

        distrib = ctrl.current_distribution
        old_activities = ctrl.group.get_activities()
        choice = ask_for_act_choice(act, authorized=["", "b", "l", "c", "r"], old_act_list=old_activities, distribution=distrib, to_show=["reference product"])
        if choice.isdigit():
            ctrl.add_new_activity(choice)
        elif choice == 'b':
            act = ctrl.remove_last_activity()
            if not ctrl.actvity_list:
                ctrl.reinit()
            print("Remouved activity:", act)
        elif choice == 'l':
            get_some_info(info, "location")
        elif choice == "c":
            get_some_info(info, "comment")
        elif choice == "r":
            if get_some_info(info, "reference product"):
                info['type'] = "process"
                print("You have given a reference product, you can give it an unit and an amount.")
                get_some_info(info, "unit")
                get_some_info(info, "production amount")
        else:
            cont = False
    
    if test:
        save_and_print_testing_result(ctrl, keep=True)
    else:
        ctrl.save_group()

    
    if export_bio:
        to_remove = check_config(ctrl.conf, "API", "EXCLUDE_BIOSPHERE", []).split(':')
        short_keys = check_config(ctrl.conf, "API", "SORT_BIOSPHERE", []).split(':')
        ctrl.export_group_data("biosphere", to_remove=to_remove, sort_keys=short_keys, scientific_notation=2, group=True)
    if export_tech:
        to_remove = check_config(ctrl.conf, "API", "EXCLUDE_TECHNOSPHERE", []).split(':')
        short_keys = check_config(ctrl.conf, "API", "SORT_TECHNOSPHERE", []).split(':')
        ctrl.export_group_data("technosphere", to_remove=to_remove, sort_keys=short_keys, scientific_notation=2)

    ctrl.reinit()

def get_biosphere_flows():
    """Offer a command line interface to export the biosphere flows of 
    an already grouped activity, in a csv file in the 'output' folder.
    """

    ctrl = Controller()

    activities = None
    while not activities:
        search = input("Give us a key-word to serarch the activity: ")

        activities = ctrl.search_modified_activities_name(search)

        if activities:
            choice = ask_for_act_choice(activities)
        else:
            print("No activity found...")
    
    act = ctrl.get_activity_from_pos(choice)
    to_remove = check_config(ctrl.conf, "API", "EXCLUDE_TECHNOSPHERE", []).split(':')
    short_keys = check_config(ctrl.conf, "API", "SORT_TECHNOSPHERE", []).split(':')

    export_biosphere_flows(act, filename=act["name"] + '.csv', to_remove=to_remove, sort_keys=short_keys, scientific_notation=2, group=True)

def ask_for_act_choice(act_list, authorized=[], old_act_list=None, distribution=None, to_show=[]):
    """Command-line interractive module to ask for a choice between a list of 
    Brightway2 activities. Returns the position of the choosen activity in the list.
    Can also return an other authorized input, given by the `authorized` keyword
    argument.

    Args:
        act_list (list): List of activities between which a choice have to be made.
            Supposed to be `Controller.current_act_set` after using 
            `Controller.find_linked_activities`.
        authorized (list, optional): List of authorized user's inputs in addition to 
            possible activity choice. Defaults to [].
        old_act_list (list, optional): If set, list of already selected activities,
            which allows to tell the user which activity in the list is an input to 
            which previously selected activity. If set, needs the `distribution` 
            argument to be also set. Supposed to be `Controller.group.activities`.
            Defaults to None.
        distribution (list, optional): Sorted list of integers, representing the 
            position in the `act_list` of the switch of previously selected activity
            in `old_act_list`, at the same position. Defaults to None.
        to_show (list, optional): List of keys of Brightway2 Activities properties, 
            which have to be displayed in addition of the default printing of
            activity. Defaults to [].

    Returns:
        str: Authorized input form the user. Ether a number representing the position 
            of his/her choice in the `act_list`, or any element of the `authorized` list.
    """
    
    to_print_act = lambda x: str(x) + ''.join([" -- " + str(s) + ": " + str(x[s]) for s in to_show if s in x])
    
    if old_act_list and distribution:
        cursor=0
        maximum = len(old_act_list)
        print("--------\n" + str(old_act_list[cursor]), "INPUT ", "\n--------")

        for i, a in enumerate(act_list):
            while cursor < maximum and distribution[cursor] == i:
                cursor += 1
                print("--------\n" + str(old_act_list[cursor]), "INPUT ", "\n--------")
            print("[", i, "] ", to_print_act(a))
    else:
        for i, a in enumerate(act_list):
            print("[", i, "] ", to_print_act(a))
    
    choice = None
    while not str(choice).isdigit() and not choice in authorized:
        choice = input("Please choose an activity (give the number): ")
        if choice.isdigit() and int(choice) >= len(act_list):
            print("Unvalid number")
            choice = None
    return choice

def get_some_info(info, info_name):
    """Command-line module that ask the user to give a value to put in
    a dictionary, at a given key. Useful to ask for metadata for the 
    grouped activity.

    Args:
        info (dict): Dictionnary in which the given information will be 
            stored. Thought as the Controller.group.info attribute.
        info_name (str): The key in the dictionnary corresponding to the
            new information. If `info[info_name]` is already set, this
            function will overwrite it.

    Returns:
        bool: True if the user gave a value to the field. In this case,
            `info` argument has been updated with the new value at the 
            `info_name` position. False if he/she skiped it by pressing enter.
            The `info` dictionary has then not been updated.
    """

    ans = input("What value do you wish to give to the field \"" + info_name + "\" (enter to escape) ? ")
    if ans:
        info[info_name] = ans 
        return True
    return False
    
def get_begin_act(ctrl, to_show=[]):
    """Command-line module that ask the user to select the terminal activity
    of the groupment he/she whishes to do. It is done by searching by key-word
    in the Ecoinvent database, then by selecting the activity in the resulting
    list.

    Args:
        ctrl (Controller): The controller used to create the new groupment.
        to_show (list, optional): List of properties of Brightway2 Activities,
            that is needed to be displayed in addition of default activities'
            printing. Defaults to [].

    Returns:
        str: Integer representing the position in `ctrl.current_act_set` of 
            the user's choice. Can be then used as `choice` argument for 
            `ctrl.validate_first_activity()`.
    """

    activities = None
    while not activities:
        search = input("Give us a key-word to serarch the begin activity: ")

        activities = ctrl.search_ecoinvent_activities_name(search)

        if activities:
            choice = ask_for_act_choice(activities, to_show=to_show)
        else:
            print("No activity found...")
    
    return choice
