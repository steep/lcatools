from os import makedirs, path
from ..utils.api_utils import sum_by_key
import csv

def export_biosphere_flows(activity, **kwargs):
    """Exports the biosphere flows of an activity as csv file,
    in the "output" folder.

    Args:
        activity (Activity): Brightway2 activity to export
        **kwargs: Keyword arguments of `flows_to_csv()` function
    """

    flows_to_csv(activity.biosphere(), **kwargs)


def export_technosphere_flows(activity, **kwargs):
    """Exports the technosphere flows of an activity as csv file,
    in the "output" folder.

    Args:
        activity (Activity): Brightway2 activity to export
        **kwargs: Keyword arguments of `flows_to_csv()` function
    """

    flows_to_csv(activity.technosphere(), **kwargs)


def flows_to_csv(iterator, filename="output.csv", 
                 to_remove=[], sort_keys=[], 
                 scientific_notation=0, group=False, 
                 discimine_by_type=False):
    """Exports the given flows of an activity to a csv file,
    exported in the 'output' folder of the program.

    A specificity of this function is its ability to add columns to 
    csv file corresponding to the sum of amounts of similar inputs.
    If desired, 6 columns will be added ('group' argument set to True):

    * sum by name: Sum amount of all elements in the file having the 
        same name
    * sum by name and category: Sum amount of all elements in the file having 
        the same name and the same categories group (eg. 'soil', 'water', 'air', 
        'natural ressouces')
    * sum by name and subcategory: Sum amount of all elements in the file having 
        the same name and the same category (tuple containing categories group and 
        category name)
    * sum by subname: Sum amount of all elements in the file having the 
        same first part of name (eg. part of the name before the first comma). 
        For instance, 'Zinc' and 'Zinc, ion' are no longer differentiated
    * sum by subname and category: Sum amount of all elements in the file having 
        the same first part of name and the same categories group
    * sum by subname and subcategory: Sum amount of all elements in the file having the 
        same first part of name and the same category
    
    If you want to use this functionality, you have to be sure that exchanges in 
    iterator contains the following keyw: `name`, `type` and `categories`. It is 
    assuerd for biosphere flows.
    
    See src.utils.api_utils.sum_by_key for further information.

    Args:
        iterator (list): Brightway2 activity's exchanges iterator whose are 
            needed to export. Activity.biosphere() or Activity.technosphere()
            for instance
        filename (str, optional): name of the output file. Defaults 
            to "output.csv".
        to_remove (list, optional): keys of informations about exchanges
            that are not desired in the exported file. Defaults to empty 
            list.
        sort_keys (list, optional): List of keys by whose the lines of 
            the csv file must be sorted. The order in list correspond to 
            the order of priority. Defaults to empty list (no sorting
            desired)
        scientific_notation (int, optional): format 'amount' (and summed) csv 
            column to scientific notation. This must be a positive or null 
            integer. If null, no scientific notation will be use, else this 
            parameter indicates the number of significant numbers desired 
            minus 1. Defaults to 0.
        group (bool, optional): if set to True, sum amount by a specific number
            of proprieties, and append the corresponding columns in output csv 
            file. WARNING: CAN BE USED ONLY WITH BIOSPHERE FLOWS AS ITERATOR.
            Defaults to False.
        discimine_by_type (bool, optional): if 'group' set to True, this indicate
            if groups have to be distinged by type (eg. 'emission' or 
            'natural ressources'). Concretely, do we sum 'emission' amount and 
            'natural ressources' amount for similar flows or not?. Defaults to False.
    
    Raises:
        AssertionError: If `categories` `name` or `type` keys ar not in all exchanges 
            metadata
    """
    
    bio = []
    for b in iterator:
        d = b.input.as_dict()
        if group:
            assert 'categories' in d
            assert 'name' in d 
            assert 'type' in d

            d['float amount'] = b.amount
        if scientific_notation and isinstance(scientific_notation, int) and scientific_notation > 0:
            to_format = "{:." + str(scientific_notation) + "e}"
            d['amount'] = to_format.format(b.amount).replace('.', ',').replace('e', 'E')
        else:
            d['amount'] = b.amount
        for r in to_remove:
            d.pop(r, None)
        bio.append(d)
        bio.sort(key=lambda x: [x[k] for k in sort_keys])

    dir_path = path.join(path.dirname(__file__), "..", "..", "output")
    if not path.isdir(dir_path):
        makedirs(dir_path)

    if bio:
        if group and ["categories", "name"] not in to_remove:
            names = ["sum by name", "sum by name and category", "sum by name and subcategory", "sum by subname", "sum by subname and category", "sum by subname and subcategory"]
            if discimine_by_type:
                sum_by_key(bio, names[0], lambda x: (x['name'], x['type']), amount_key_name='float amount')
                sum_by_key(bio, names[1], lambda x: (x['name'], x['categories'][0], x['type']), amount_key_name='float amount')
                sum_by_key(bio, names[2], lambda x: (x['name'], x['categories'], x['type']), amount_key_name='float amount')
                sum_by_key(bio, names[3], lambda x: (x['name'].split(',')[0], x['type']), amount_key_name='float amount')
                sum_by_key(bio, names[4], lambda x: (x['name'].split(',')[0], x['categories'][0], x['type']), amount_key_name='float amount')
                sum_by_key(bio, names[5], lambda x: (x['name'].split(',')[0], x['categories'], x['type']), amount_key_name='float amount')
            else:
                sum_by_key(bio, names[0], lambda x: x['name'], amount_key_name='float amount')
                sum_by_key(bio, names[1], lambda x: (x['name'], x['categories'][0]), amount_key_name='float amount')
                sum_by_key(bio, names[2], lambda x: (x['name'], x['categories']), amount_key_name='float amount')
                sum_by_key(bio, names[3], lambda x: x['name'].split(',')[0], amount_key_name='float amount')
                sum_by_key(bio, names[4], lambda x: (x['name'].split(',')[0], x['categories'][0]), amount_key_name='float amount')
                sum_by_key(bio, names[5], lambda x: (x['name'].split(',')[0], x['categories']), amount_key_name='float amount')
            for b in bio:
                b.pop('float amount', None)
                if scientific_notation:
                    for n in names:
                        to_format = "{:." + str(scientific_notation) + "e}"
                        b[n] = to_format.format(b[n]).replace('.', ',').replace('e', 'E')


        keys = bio[0].keys()
        
        with open(path.join(dir_path, filename), 'w', newline='') as f:
            dict_writer = csv.DictWriter(f, keys, delimiter=";")
            dict_writer.writeheader()
            dict_writer.writerows(bio)
    else:
        with open(path.join(dir_path, filename), 'w', newline='') as f:
            f.write("No biosphere flows")

