import brightway2 as bw2
import networkx as nx
import numpy as np
from os import path
from math import sqrt

"""Position of activity code in Brightway2 activity's key tuple"""
KEY_CODE = 1

"""Position of activity database in Brightway2 activity's key tuple"""
KEY_DATABASE = 0

def activity_key(activity):
    """Getter for a Brightway2 activity key
    
    Args:
        activity (Activity): activity whose the 
            key will be returned
    
    Returns:
        tuple: activity key, tuple containing database 
            name and activity code
    """

    return activity.key

def exchange_input_key(exchange):
    """Getter for a Brightway2 exchange input activity key
    
    Args:
        exchange (Exchange): exchange whose the key of the 
            input activity will be returned
    
    Returns:
        tuple: input activity key, tuple containing database 
            name and activity code
    """

    return exchange['input']

def exchange_output_key(exchange):
    """Getter for a Brightway2 exchange output activity key
    
    Args:
        exchange (Exchange): exchange whose the key of the output 
            activity will be returned
    
    Returns:
        tuple: output activity key, tuple containing database 
            name and activity code
    """

    return exchange['output']

def exchange_links(exchange, activity_set):
    """Tests if a Brightway2 specific exchange have both of
    its input and output activities inside a given list of 
    activities.

    Info:
        Useful to verify if an exchange is inside a group
        of activities

    Args:
        exchange (Exchange): Brightway2 exchange that we want 
            to verify if it is internal of the activities set
        activity_set (list): List of activities in which we want 
            to verify if exchange have its two extremities

    Returns:
        bool: True if the test passed, else False
    """

    activities_keys = [activity_key(act) for act in activity_set]
    if exchange_input_key(exchange) in activities_keys and exchange_output_key(exchange) in activities_keys:
        return True 
    return False

def get_activity_by_key(activity_key):
    """Take an activity key and returns the associated 
    Brightway2 Activity object

    Args:
        activity_key (tuple): 2D-tuple corresponding to
            an activity key (eg. database name and 
            activity code)

    Returns:
        Activity: If the activity exists in the current 
            Brightway2 setup, returns the activity
            associated to the given key. Else None
    """

    database = activity_key[KEY_DATABASE]
    code = activity_key[KEY_CODE]
    if database in bw2.databases:
        activity = bw2.Database(database).get(code)
        if activity:
            return activity
        return None
    return None

def detect_multiple_input(exch_list):
    """Tell if inside a list of Brightway2 exchanges,
    some of them point to the same activity

    Args:
        exch_list (list): list of Brightway2 exchanges

    Returns:
        bool: True if the list contains exchanges that
            have the same output activity, else False
    """

    final_list = [e for e in exch_list if e.input != e.output]
    for e1 in final_list:
        for e2 in final_list:
            if e1 is e2:
                continue 
            if e1.output == e2.output:
                return True 
    return False

def get_graph(exchanges, name="Activities graph"):
    """Turn a set of Brightway2 exchanges to a networkx
    DiGraph (directed graph), representing the group of 
    activities linked by the exchanges.

    Args:
        exchanges (list): list of Brightway2 exchange that
            needs to be turned to networkx.DiGraph
        name (str, optional): Name of the graph. Defaults to 
            "Activities graph".

    Returns:
        DiGraph: Directed graph representing the list of 
            exchanges
    """

    graph = nx.DiGraph(name=name)
    for e in exchanges:
        if e.input != e.output or abs(e.amount) != 1 or e['type'] != 'production':
            graph.add_edge(e.input, e.output, label=e.amount)
    return graph

def save_graph(graph, dir_path,  img_format='png', 
               output_activity=None, output_name="output"):
    """Save a networkx DiGraph representing the network of 
    a group of Brightway2 activities to an image file.

    Args:
        graph (DiGraph): DiGraph to export (can be built with the function
            'get_graph')
        dir_path (str): Path to the output image (no verification that the 
            extension matches the image format is made)
        img_format (str, optional): image format for the exported file. 
            Defaults to 'png'.
        output_activity (Activity, optional): If indicated, this will show
            the output activity on the graph by adding a terminal node
            colored in red and called by the string of the 'output_name'
            argument. Defaults to None.
        output_name (str, optional): label of the output node of the graph, 
            representing the output of the group of activities. Useful only
            if 'output_activity' is indicated. Defaults to "output".

    Returns:
        str: path to the saved image
    """

    g = nx.nx_agraph.to_agraph(graph)
    if output_activity:
        g.node_attr['style'] = 'filled'
        g.add_node(output_name, color='white', fillcolor='red')
        g.add_edge(output_activity, output_name, label=output_activity["production amount"])
    g.layout(prog='dot')

    filename = path.join(dir_path , graph.name + "." + img_format)    
    g.draw(path=filename, format=img_format) 

    return transform_to_shorted_path(filename)

def transform_to_shorted_path(full_path):
    """Turn a string representing a path in directories
    to its equivalent without backtrack (eg by removing 
    "..").

    Args:
        full_path (str): Path to reduce

    Returns:
        str: Path reduced
    """

    elements = full_path.split(path.sep)
    new_elements = []
    for i, e in enumerate(elements):
        if e == "..":
            if len(new_elements) != 0:
                new_elements.pop()
            else:
                new_elements.append(e)
        elif e != ".":
            new_elements.append(e)
    final_path = path.join(*new_elements)
    if len(full_path) > 0 and full_path[0] == path.sep:
        final_path = path.sep + final_path
    return final_path

def check_config(config, section, value, default_value, type=str):
    """Verify if a configuration element is indeed configured,
    change its type from string to the desired type and returns it.
    If the element isn't referenced in the configuration, use a default
    value.

    Info:
        Log a warning in the logger if the desired value is not 
        configured as it should be

    Args:
        config (ConfigParser): parsed ConfigParser element, supposed
            to contain the desired value
        section (str): section of the ConfigParser element containing 
            the desired value
        value (str): name of the value desired
        default_value (any): value to return if the desired value 
            isn't configured. This value will not be casted in the 
            desired type, must already be in the desired type
        type (type, optional): type in which the desired value have to 
            be. Defaults to str.

    Returns:
        any: the desired configuration value, in the desired type
    """

    try:
        if type == bool:
            val = config[section].getboolean(value)
        else:
            val = config[section][value]
            val = type(val)
    except KeyError:
        val = default_value
        logging.warning("No value for " + str(value) + " in " + str(section) + " section of 'config.ini' file. Default value given: " + str(val))
    except ValueError:
        val = default_value 
        logging.warning("Non valid value-type for " + str(value) + " in " + str(section) + " section of 'config.ini' file. Default value given: " + str(val))
    return val
