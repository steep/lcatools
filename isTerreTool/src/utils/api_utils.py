
def sum_by_key(dict_list, sum_name, key, amount_key_name='amount'):
    """Take a list of dictionaries having the same keys 
    and sum one element of the dictionaries grouped by a specific
    element or group of elements. Each dictionary gets a new column
    corresponding to the calculated sum.

    Exemple:
        ```python
        dict01 = {"name": "Flower", "color": "Green", "amount": 3}
        dict02 = {"name": "Rock", "color": "Green", "amount": 2}
        dict03 = {"name": "Flower", "color": "Red", "amount": 6}

        dict_list = [dict01, dict02, dict03]

        sum_name = "Sum by name"
        key = lambda x: x["name"]
        amount_key_name = 'amount'

        sum_by_key(dict_list, sum_name, key, amount_key_name)

        sum_name = "Sum by color"
        key = lambda x: x["color"]

        sum_by_key(dict_list, sum_name, key, amount_key_name)

        print("dict01:", dict01)
        print("dict02:", dict02)
        print("dict03:", dict03)
        ```

        ```OUTPUT
        dict01: {"name": "Flower", 
                 "color": "Green", 
                 "amount": 3,
                 "Sum by name": 9, 
                 "Sum by color": 5}
        
        dict02: {"name": "Rock", 
                 "color": 
                 "Green", 
                 "amount": 2
                 "Sum by name": 2, 
                 "Sum by color": 5}
        
        dict03 = {"name": "Flower", 
                  "color": "Red", 
                  "amount": 5
                  "Sum by name": 9, 
                  "Sum by color": 6}
        ```

    Args:
        dict_list (list): list of similar dictionaries, that we can group given
            a specific property ('key' argument), and which contain a value to
            sum.
        sum_name (str): name of the new element of dictionaries, which corresponds
            to the summed value
        key (function): function that, given a dictionary, extract the key by which 
            dictionnaries will be grouped
        amount_key_name (str, optional): Name of the element of the dictionaries 
            that we are looking to sum. Defaults to 'amount'.
    """

    dealed = dict()
    for d in dict_list:
        if key(d) in dealed:
            d[sum_name] = dealed[key(d)]
        else:
            val = sum(x[amount_key_name] for x in dict_list if key(x) == key(d))
            dealed[key(d)] = val
            d[sum_name] = val 
