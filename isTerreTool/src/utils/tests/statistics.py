import matplotlib.pyplot as plt 
import numpy as np
from math import log10, sqrt
from scipy.stats import chisquare, norm
from ..dataobj import transform_to_shorted_path

def to_one_digit_benfords_law(vect, save_path=None, title=None):
    """Take a list of numbers and returns the chi2 and p-value associated 
    of the Benford's law test for 1 significant digit.

    Args:
        vect (list): list or iterable object of the numbers to test. Type of elements
            doesn't matter as long as thoses elements can be turned to strings
        save_path (str, optional): if not null or None, allow the function to save 
            the graph of first digit comparaison at the location indicated by this 
            argument (must be valable)
        title (str, optional): If save_path given, add a title to the graph
    
    Returns:
        _tuple_

        _If vect argument contains only null elements_
            
        * None 
        * None 
        
        _Else_
            
        * float: chi-squared value of Benford's law chi-squared test
        * float: p-value of Benford's law chi-squared test
    """

    val = [0] * 9
    for v in vect:
        numeric_filter = filter(str.isdigit, str(v))
        str_v = "".join(numeric_filter)
        for i in range(len(str_v)):
            f_digit = int(str_v[i])
            if f_digit != 0:
                val[f_digit - 1] += 1
                break
    
    size = sum(val)

    if not size:
        return None, None
    
    # Chi squared test
    chi, p_value = chisquare([v/size for v in val], [log10(1 + 1/a) for a in range(1, 10)])

    # Save plot
    if save_path:
        plt.bar(range(1, 10), val, label="found", color='black')
        plt.plot(np.arange(1, 9, 0.01), [size*log10(1 + 1/a) for a in np.arange(1, 9, 0.01)], label="expected", color='red')
        plt.legend()
        if title:
            plt.title(title)
        plt.savefig(save_path)

        plt.clf()
        plt.cla()
        plt.close()

    return chi, p_value

def gaussian_qqplot(vector, save_path, title="Gaussian QQ-plot"):
    """Take a vector of values, then create and save the corresponding 
    gaussian QQ-plot. Can be usefull to dect anomalies.

    Args:
        vector (list): list of values to create the gaussian QQ-plot
        save_path (str): path to te futur saved image of plot. Must 
            be valid
        title (str, optional): The title of the graph to print on the 
            image. Defaults to "Gaussian QQ-plot".
    """

    new_vector = vector
    size = len(new_vector)
    stats = get_statistics_from_vector(new_vector)
    normal = norm(loc=stats["Mean"], scale=sqrt(stats["Variance"]))
    shaped = [a/size for a in range(size)]

    ppfs = []
    for s in shaped:
        p = normal.ppf(s)
        ppfs.append(p)

    new_vector.sort()

    plt.plot(ppfs, ppfs, color="black", label="Expected", linewidth=0.5)
    plt.plot(ppfs, new_vector, "+", color='red', label="Data")
    plt.xlabel("Reference quantiles")
    plt.ylabel("Calculated quantiles")
    plt.legend()
    plt.title(title)
    plt.savefig(save_path)
        
    plt.clf()
    plt.cla()
    plt.close()


def get_statistics_from_vector(vect, save_path=None, name=None):
    """Create a dictionary of statistics from a given vector.

    Returned dict key and associated values:

    * "Mean": mean of vector
    * "Median": median value of vector
    * "1st quantile": quantile at 25% of vector
    * "3rd quantile": quantile at 75% of vector
    * "Variance": variance of vector
    * "Maximum": maximum value of vector
    * "Minimum": minimum value of vector
    * "Maximum position": position in vector of maximum value
    * "Minimum position": position in vector of minimum value
    * "Benford p-value": p-value of Benford's law chi-squared test 

    Info:
        Is used to create the 2 first tests' statistic dictionaries, 
        which will be then be verified with the Checker class (keys 
        of Checker.absolute and Checker.relative dictionaries are 
        based on some of the keys of the dict returned by this 
        function)

    Args:
        vect (list): list of numbers to extract statistics
        save_path (str, optional): If not None, will save the Benford's
            law plot at the specified position. Must be valid. Defaults 
            to None.
        name (str, optional): If not None and 'save_path' nither, will 
            print this name to the saved Benford's law plot image. 
            Defaults to None.

    Returns:
        dict: dictionary of vector's statistics
    """

    stats = dict()
    if len(vect):
        stats["Mean"] = np.mean(vect)
        stats["Median"] = np.median(vect)
        stats["1st quantile"] = np.quantile(vect, 0.25)
        stats["3rd quantile"] = np.quantile(vect, 0.75)
        stats["Variance"] = np.var(vect)
        stats["Maximum"] = np.max(vect)
        stats["Minimum"] = np.min(vect)
        stats["Maximum position"] = vect.index(stats["Maximum"])
        stats["Minimum position"] = vect.index(stats["Minimum"])
        if name:
            stats["Benford p-value"] = to_one_digit_benfords_law(vect, save_path=save_path, title=name + " Benford's law")[1]
        else:
            stats["Benford p-value"] = to_one_digit_benfords_law(vect, save_path=save_path)[1]

    return stats

def graph_ref_found(comparator, path, title=None):
    """Create a graph of calculated values in function of reference values, 
    and save it as image.
    Useful for qualitative verification of testing results.

    Info:
        The graph scale is logarithmic, so the values of the comparator 
        are turned to absolute. If only null values are presented, the 
        graph is swiched to arithmetic scale (but the graph become trivial 
        of course)

    Args:
        comparator (list): list of 2D-tuples. The first element of every
            tuple must be the reference value, and the second element must
            be the found value to compare to the reference.
        path (str): path to the output image of the function
        title (str, optional): If not None, print given title on the to of 
            the generated image. Defaults to None.

    Returns:
        str: path to generated graph image
    """

    ref_list = [abs(c[0]) for c in comparator]
    found_list = [abs(c[1]) for c in comparator]
    
    plt.plot(ref_list, ref_list, color="black", label="Expected", linewidth=0.5)
    plt.plot(ref_list, found_list, "+", color='red', label="Data")
    plt.xlabel("Reference values")
    plt.ylabel("Calculated values")
    if len([True for a, b in zip(ref_list, found_list) if a > 0 and b > 0]) > 0:
        plt.xscale("log")
        plt.yscale("log")
    plt.legend()
    if title:
        plt.title(title)
    plt.savefig(path)

    plt.clf()
    plt.cla()
    plt.close()

    return transform_to_shorted_path(path)

def construct_comparator(lca01, lca02):
    """Take two Brightway2 LCA elements and construct a list 
    of 2D-tuples containing the values of each element that are
    in both LCA inventories (biosphere flow).

    That allows to compare the LCI of both LCA elements, biosphere
    flow value by biosphere flow value.

    Info:
        The call to LCA.lci() method must be done before calling 
        this function.

        Warning: this function doesn't keep the information about 
        which tuple matches which biosphere flow. It juste create
        list of tuple containing comparable values.

    Args:
        lca01 (LCA): First LCA element to compare
        lca02 (LCA): Second LCA element to compare

    Raises:
        AssertionError: If at least one of the to LCA object given 
            haven't done their LCA.lci() calculation before
    
    Returns:
        list: List of 2D-tuple containing comparable values
    """

    assert not lca01.inventory is None
    assert not lca02.inventory is None
    assert lca01._fixed
    assert lca02._fixed

    bio01 = lca01.inventory.sum(axis=1)
    bio02 = lca02.inventory.sum(axis=1)
    
    comparator = []
    for k in lca01.biosphere_dict:
        if k in lca02.biosphere_dict:
            pos01 = lca01.biosphere_dict[k]
            pos02 = lca02.biosphere_dict[k]
            comparator.append((bio01[pos01, 0], bio02[pos02, 0]))
    
    return comparator

