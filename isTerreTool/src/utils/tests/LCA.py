"""Warning: Elements of this file are no longer used in the final code"""

from math import sqrt
from scipy.sparse import spdiags
from bw2calc.matrices import TechnosphereBiosphereMatrixBuilder as TBMBuilder
from bw2calc.lca import LCA

class Test_LCA(LCA):
    """ An alternative LCA class which try to get better consitancy of 
    LCA' matrices before calculation, by centering units around an amount of 1
    """

    def lci(self, factorize=False,
            builder=TBMBuilder, bio_factors=None):
        """Re-writed lci function of Brightway2's LCA class to try to 
        get a better condition number for biosphere matrix by centering 
        rows around 1.

        See https://2.docs.brightway.dev/technical/bw2calc.html#bw2calc.LCA.lci
        for elements of documentation that concerne the original method

        Args:
            bio_factors (csr_matrix, optional): Diagonal csr_matrix reprsenting 
                the multiplicative factors to apply to the biosphere matrix. If given, 
                it will be the base of the biosphere's matrix change of unit. Else, this 
                matrix will be built from the start. Useful for test comparaison: 
                factors in the two configuration have to be exactly the same. Defaults to None.

        Returns:
            csr_matrix: At the difference of the original method, a value is returned, 
                and this value is the diagonal matrix of multiplicative factors of 
                biosphere's matrix. It permits to be reused as parameter for the lci 
                function of an other Test_LCA object (to compare the results)
        """
        
        self.load_lci_data(builder=builder)

        # Modify biosphere matrix before calc
        fact_bio, self.biosphere_matrix = change_matrix_rows_unit(self.biosphere_matrix, factors=bio_factors)
    
        self.build_demand_array()
        if factorize:
            self.decompose_technosphere()
        self.lci_calculation()

        return fact_bio


def change_matrix_rows_unit(matrix, factors=None):
    """Center rows min and max values of a matrix around 1, to 
    limit float errors in future arithmetics.
    Returns also the matrix of correction factors.

    Args:
        matrix (csr_matrix): the matrix to balance
        factors (csr_matrix): if None, the factors of correction will be computed, 
            else, it will be taken as the matrix of correction.
            Must be diagonal. If the shapes between matrix and 
            factors don't match for multiplication, the shape of
            the factor's matrix will be adapted.

    Returns:
        _tuple containing_

        * csr_matrix: diagonal matrix of applied factors
        * csr_matrix: the balanced matrix
    """

    #Create matrix of unit changes
    if factors is None:
        min_vect, max_vect = matrix_row_min_max(matrix)
        vect = []
        for i, j in zip(min_vect, max_vect):
            if i*j:
                vect.append(1/sqrt(i*j))
            else:
                vect.append(1)
        factors = spdiags(vect, 0, len(vect), len(vect))
    else:
        shape_matrix = matrix.get_shape()
        shape_factors = factors.get_shape()

        if shape_factors[1] != shape_matrix[0]:
            factors.resize((shape_matrix[0], shape_matrix[0]))
            if shape_matrix[0] > shape_factors[1]:
                min_vect, max_vect = matrix_row_min_max(matrix)
                for i in range(shape_factors[1], shape_matrix[0]):
                    factors[i, i] = 1/sqrt(min_vect[i]*max_vect[i])
        
    # Change unit
    new_matrix = factors.dot(matrix)
    return factors, new_matrix


def matrix_row_min_max(matrix):
    """Take a numpy csr_matrix, and return a list representing
    the absolute non-zero minimums of every rows and a list representing 
    the absolute maximums of every row. If an element of the returned lists 
    is equals to 0, then it means that the corresponding raw of the matrix
    was all null.

    Args:
        matrix (csr_matrix): the matrix to get the absolute min 
            and max of every rows
    
    Returns:
        _tuple containing_

        * list: list of matrix's rows absolute non-null (except 
            if all values of the row are equals to 0) minimum values
        * list: list of matrix's rows absolute maximum values
    """

    non_zero_indices = matrix.nonzero()
    non_zero_bio = []
    prev = None
    for i, j in enumerate(non_zero_indices[0]):
        if j != prev:
            if i != 0:
                non_zero_bio.append(new_list)
                if prev != j - 1:
                    for _ in range(prev, j):
                        non_zero_bio.append([0])
            new_list = []
            prev = j
        new_list.append(abs(matrix[j, non_zero_indices[1][i]]))
    max_list = [max(b) for b in non_zero_bio]
    min_list = [min(b) for b in non_zero_bio]

    return min_list, max_list
