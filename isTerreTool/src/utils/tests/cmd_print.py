from colorama import init 
from termcolor import colored
from .checker import Checker

"""Tests' statistics are distributed in 3 distincts dictionaries,
always shorted the same way. 'STATS_NAMES' representes the names
of thoses dictionaries given their position in the list.
"""
STATS_NAMES = ["Absolute errors", "Relative errors", "Others"]    

def print_checked_dict(to_check, checker_dict):
    """Print to standard output all the statistics of a given 
    statistic dictionary, colored in:

    * White: if no test is setup for the given statistic, or 
        if the test didn't detect any anomaly.
    * Yellow: if a warning has been detected by the test
    * Red: if an error has been detected by the test

    Info:
        The use of this fuction assumed that the terminal in which 
        the application is used supports color dispaly
    
    Args:
        to_check (dict): dictionary of statistics to check
        checker_dict (dict): Checker's associated dictionary
    """

    init()
    for k in to_check:
        color = 'white'
        if k in checker_dict:
            val = checker_dict[k][Checker.TEST_POSITION](to_check[k])
            if val == 0:
                color = 'white'
            elif val == 1:
                color = 'yellow'
            else:
                color = 'red'
        print(colored("[ " + str(k) + " ] " + str(to_check[k]), color))

def write_title(title, file=None):
    """Write a string in a specific format, supposed to be a title.
    
    The format is the following:

    ```text
    --------------
    My Title
    --------------
    ```

    Args:
        title (str): string to print
        file (File, optional): if None, the title will be printed 
            in the standard output. Else, the title will be writed 
            in this given file. Defaults to None.
    """

    if file is None:
        print("--------------")
        print(title)
        print("--------------")
    else:
        file.write("--------------\n")
        file.write(str(title) + "\n")
        file.write("--------------\n")

def write_dict(dictionnary, file=None):
    """Write a dictionary's elements in a specific format, the one 
    under the other.
    
    The format is the following:

    ```text
    [ key1 ] value1
    [ key2 ] value2
    ...
    [ keyN ] valueN
    ```

    Args:
        dictionnary (str): the dictionary to print
        file (File, optional): if None, the dictionary will be 
            printed in the standard output. Else, the dictionary will
            be writed in this given file. Defaults to None.
    """

    if file is None:
        for k in dictionnary:
            print("[", k, "]", dictionnary[k])
    else:
        for k in dictionnary:
            file.write("[ " + str(k) + " ] " + str(dictionnary[k]) + "\n")

def save_and_print_testing_result(ctrl, keep=False, verbose=False):
    """This function is quite special, because it mixes controller's
    functionalities and command-line interface. It will take a controller 
    with a setup group, save the group, and test it. It will print in the 
    standard output if the test has been successfull.

    Info:
        This function kind of does not respect the MCV architecture of global 
        code, but is usefull to factorize this type of command-line testing 
        display. For instance, it is used for automatic testing and for data 
        verification after groupment with the command-line interface.

    Args:
        ctrl (Controller): The Controller's instance containing the groupment 
            to test.
        keep (bool, optional): If set to True, the tested activity will be
            kept in the database after testing. Else it will be removed 
            to free space. Defaults to False.
        verbose (bool, optional): If set to true, the test will print in the 
            standard output all the test' statistics using the 'print_checked_dict'
            function. Else, it will only print if a problem has been detected or not.
            Defaults to False.
    """

    code, elements = ctrl.save_and_test(keep=keep, verbose=verbose)
    if code == 0:
        print(colored("No error detected!", 'green'))
    elif code == 1:
        print(colored("WARNING: Uncertaintie(s) detected in result, nothing to worry", 'yellow'))
        for w in elements:
            print("Uncertainitie:", STATS_NAMES[w[0]], "-", w[1], "-", w[2])
    else:
        print(colored("PROBLEM: Anomaly(ies) detected in result", 'red'))
        for e in elements:
             print("Anomaly:", STATS_NAMES[e[0]], "-", e[1], "-", e[2])
        print("Starting Monte Carlo verification")
        if ctrl.verify_errors_stability(elements):
            print(colored("False alert, everything is fine", 'green'))
        else:
            print(colored("A real problem has been detected, please check manualy data before using it.", 'red'))
