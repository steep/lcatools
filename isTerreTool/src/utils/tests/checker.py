import numpy as np
from scipy.stats import mstats
from ..dataobj import check_config

class Checker():
    """The Checker class is used to check if specific statistics of 
    an app test are considered as 'normal' or 'anormal' (eg. might 
    have not trust-worthy results). 

    Thoses statistics are classified in 3 categories:

    * Statistics on absolute errors vector
    * Statistics on relative errors vector
    * Other statistics

    Thoses categories are represented by dictionaries.
    Each of thoses dictionary are structured as follows:

    * Each key of the dictionary represents a specific 
      statistic. For practical reasons, thoses keys must 
      be identical of thoses of the statistic dictionaries
      to check.
    * Elements of the dictionaries are 2D-tuples of functions:
        * The first (TEST_POSITION static attribute) element is a
          function which takes in argument the value of the statistic
          to check and returns 0 if the test passed, 1 if a warning must 
          be rised, and 2 if an anomaly is detected. This function is
          called the audit function.
        * The second (MEAN_POSITION static attribute) element is a
          function which takes in argument a vector of the statistic 
          to check (calculated multiple times by Monte Carlo approach
          for example), and returns an unique value to check (arithmetic
          or geometric means of vector for instance)


    Attributes:
        TEST_POSITION (int, static): The position in dictionaries' elements
            tuples of the audit function
        MEAN_POSITION (int, static): The position in dictionaries' elements
            tuples of the function which gives from a vector of the associated 
            statistic a mean value
        config (ConfigParser): A configuration dictionary with a 'TEST' section
            containing the cutoff associated with differents statistics (see 
            'config.ini' of isTerreTool project)
        absolute (dict): Dictionary of tests for absolute errors statistics
        relative (dict): Dictionary of tests for relative errors statistics
        others (dict): Dictionary of tests for non absolute errors nither 
            relative errors statistics
        list (list): List of the three dictionaries listed above
    """

    TEST_POSITION = 0
    MEAN_POSITION = 1

    def __init__(self, config):
        """Create an instance of Chercker: construct initilize and 
        construct its internal dictionaries.

        Args:
            config (ConfigParser): A configuration dictionary with a 
                'TEST' section containing the cutoff associated with 
                differents statistics (see 'config.ini' of isTerreTool 
                project)
        """

        self.config = config
        self.absolute = dict()
        self.relative = dict()
        self.other = dict()

        self.list = [self.absolute, self.relative, self.other]

        self._construct_dicts()

    def _construct_dicts(self):
        """Construct Checker's internal dictionaries with private 
        internal functions
        """

        self.relative['Maximum'] = (self._relative_max_check, mstats.gmean)
        self.relative['Benford p-value'] = (self._relative_benford_check, np.mean)
        self.absolute['Maximum'] = (self._absolute_max_check, mstats.gmean)
        self.absolute['Benford p-value'] = (self._absolute_benford_check, np.mean)
        self.absolute['Minimum'] = (self._absolute_min_check, mstats.gmean)
        self.other['Singularity'] = (self._singularity_check, np.mean)

    def _relative_max_check(self, value):
        """Audit function of the maximal value of 
        relative errors' vector:

        * Pass: the value is under 'RELATIVE_TOLERANCE_FACTOR' 
            of 'TEST' section in self.config
        * Warning: the value is between 'RELATIVE_TOLERANCE_FACTOR' 
            of 'TEST' section in self.config and 
            'RELATIVE_EXTREME_TOLERANCE_FACTOR' of 'TEST' section 
            in self.config
        * Don't pass: the value is above 'RELATIVE_EXTREME_TOLERANCE_FACTOR' 
            of 'TEST' section in self.config

        Args:
            value (float): the maximal value of the relative errors vector 
                in test statistics. If None, the test is considered
                passed

        Returns:
            int: 0 if the test passed, 1 if a warining must be rised, 2 if 
                the value isn't valid
        """

        limit = check_config(self.config, "TEST", "RELATIVE_TOLERANCE_FACTOR", 0.001, type=float)
        extreme_limit = check_config(self.config, "TEST", "RELATIVE_EXTREME_TOLERANCE_FACTOR", 0.1, type=float)
        if value is None:
            return 0
        if value < limit:
            return 0
        elif value > extreme_limit:
            return 2
        return 1
    
    def _absolute_max_check(self, value):
        """Audit function of the maximal value of 
        absolute errors' vector:

        * Pass: the value is under 'ABSOLUTE_TOLERANCE_FACTOR' 
            of 'TEST' section in self.config
        * Warning: the value is between 'ABSOLUTE_TOLERANCE_FACTOR' 
            of 'TEST' section in self.config and 
            'ABSOLUTE_EXTREME_TOLERANCE_FACTOR' of 'TEST' section 
            in self.config
        * Don't pass: the value is above 'ABSOLUTE_EXTREME_TOLERANCE_FACTOR' 
            of 'TEST' section in self.config

        Args:
            value (float): the maximal value of the absolute errors vector 
                in test statistics. If None, the test is considered
                passed

        Returns:
            int: 0 if the test passed, 1 if a warining must be rised, 2 if 
                the value isn't valid
        """

        limit = check_config(self.config, "TEST", "ABSOLUTE_TOLERANCE_FACTOR", 0.1, type=float)
        extreme_limit = check_config(self.config, "TEST", "ABSOLUTE_EXTREME_TOLERANCE_FACTOR", 100, type=float)
        if value is None:
            return 0
        if value < limit:
            return 0
        elif value > extreme_limit:
            return 2
        return 1
    
    def _relative_benford_check(self, value):
        """Audit function of the p-value of Benford's law chi-squared
        test on relative errors' vector:

        * Pass: the value is above 'RELATIVE_BENFORD_TOLERANCE_FACTOR' 
            of 'TEST' section in self.config
        * Warning: the value is under 'RELATIVE_BENFORD_TOLERANCE_FACTOR' 
            of 'TEST' section in self.config

        Info: 
            Observing Benford's law in relative error vector might not be 
            relevent. This function is there in case of, but it is recomanded
            to have a 'RELATIVE_BENFORD_TOLERANCE_FACTOR' very low.

        Args:
            value (float): Benford's law chi-squared test p-value of relative
                errors vector in test statistics. If None, the test is considered
                passed

        Returns:
            int: 0 if the test passed, 1 if a warining must be rised
        """

        limit = check_config(self.config, "TEST", "RELATIVE_BENFORD_TOLERANCE_FACTOR", 0, type=float)
        if value is None:
            return 0
        if value < limit:
            return 1
        return 0

    def _absolute_benford_check(self, value):
        """Audit function of the p-value of Benford's law chi-squared
        test on absolute errors' vector:

        * Pass: the value is above 'ABSOLUTE_BENFORD_TOLERANCE_FACTOR' 
            of 'TEST' section in self.config
        * Warning: the value is under 'ABSOLUTE_BENFORD_TOLERANCE_FACTOR' 
            of 'TEST' section in self.config

        Args:
            value (float): Benford's law chi-squared test p-value of absolute
                errors vector in test statistics. If None, the test is considered
                passed

        Returns:
            int: 0 if the test passed, 1 if a warining must be rised
        """

        limit = check_config(self.config, "TEST", "ABSOLUTE_BENFORD_TOLERANCE_FACTOR", 0.9999, type=float)
        if value is None:
            return 0
        if value < limit:
            return 1
        return 0
    
    def _absolute_min_check(self, value):
        """Audit function of the minimal value of absolute 
        errors' vector:

        * Pass: the value is under 'ABSOLUTE_MIN_TOLERANCE_FACTOR' 
            of 'TEST' section in self.config
        * Warning: the value is above 'ABSOLUTE_MIN_TOLERANCE_FACTOR' 
            of 'TEST' section in self.config

        Info:
            On huge enough vector, it is resonable to consider 
            'ABSOLUTE_MIN_TOLERANCE_FACTOR' equals to 0

        Args:
            value (float): Minimal value of absolute errors vector 
            in test statistics. If None, the test is considered
            passed

        Returns:
            int: 0 if the test passed, 1 if a warining must be rised
        """

        limit = check_config(self.config, "TEST", "ABSOLUTE_MIN_TOLERANCE_FACTOR", 0, type=float)
        if value is None:
            return 0
        if value > limit:
            return 1
        return 0
    
    def _singularity_check(self, value):
        """Audit function of the number of singulatrities 
        in test's comparaison (eg. every time when a non-negligible 
        value is found in one of the two vectors -reference and calculated-
        where the other vector gives at the same place a null value):

        * Pass: the value is under 'SINGULARITY_TOLERANCE_FACTOR' 
            of 'TEST' section in self.config
        * Don't pass: the value is above 'SINGULARITY_TOLERANCE_FACTOR' 
            of 'TEST' section in self.config

        Args:
            value (float): Number of singulatrities detected in test 
            statistics. If None, the test is considered passed

        Returns:
            int: 0 if the test passed, 2 if the value isn't valid 
        """

        limit = check_config(self.config, "TEST", "SINGULARITY_TOLERANCE_FACTOR", 0.9, type=float)
        if value is None:
            return 0
        if value > limit:
            return 2
        return 0
    

def check_level(to_check_list, checker_dict_list):
    """This function takes as arguments a list of a test's statistic 
    dictionaries (for instance statistics about the vector of absolute
    errors, the vector of relative errors and others statistics), and 
    the associated list of Checker's internal dictionaries. Thoses two 
    lists must be shorted the same way and have the same lenght.

    It will verify each test in the list of Checker's internal dictionary,
    and every time an anomaly is detected (error or warning, eg. return 
    value of Checker's functions equals to 1 or 2), it will save it in 
    two lists (one for warnings and one for errors).

    In thoses lists, a warning or an error is represented by a 3D-tuple
    containing:

    * The position of the dictionary associated in given lists
    * The name of the key in the dictionary
    * The value of the anomaly

    Info:
        Even if lists ginven as parameters must have the same lenght, 
        the dictionaries in thoses list don't have to be of the same size,
        but only keys in common (of a dictionary in one list and the 
        dictionary in the other list at the same position) will be tested

    Args:
        to_check_list (list): list of dictionaries of statistics about a 
            specific test
        checker_dict_list (list): list of Checker's internal dictionnaries
            corresponding to the list of statistics to test (usually 
            Checker.list)
    
    Raises:
        AssertionError: If the two lists given as arguments don't have the 
            same lenght

    Returns:
        _tuple containing_

        * list: list of warnings (3D-tuples) detected
        * list: list of errors (3D-tuples) detected
    """

    assert len(to_check_list) == len(checker_dict_list)
    warnings = []
    errors = []
    for i, (to_check, checker) in enumerate(zip(to_check_list, checker_dict_list)):
        for k in to_check:
            if k in checker:
                val = checker[k][Checker.TEST_POSITION](to_check[k])
                if val == 1:
                    warnings.append((i, k, to_check[k]))
                elif val >= 2:
                    errors.append((i, k, to_check[k]))
    
    return warnings, errors

