import logging
import brightway2 as bw2
import os
from configparser import ConfigParser
from ..model.group import Activity_group
from ..utils.dataobj import *
from ..utils.tests import *
from ..API.data import export_biosphere_flows, export_technosphere_flows

class Controller:
    """Controller of the MVC architecture of app. Controller class
    links interface with model.

    Controller instance have to be used as a singleton (https://en.wikipedia.org/wiki/Singleton_pattern).
    This means that if multiple instances of this class are created, thoses are all
    aliases for an unique instance (the first created).
    The constraint of the singleton will probably have to be lifted in the futur, if concurrent access
    needs to be managed (in web interface for instance)

    This class has been thought as the one that treat informations coming from the
    interface and use it to generate a grouped set of activities.

    Exemples:
        Creating a random 3-activities group from an activity associated with 
        'banana production'

        ```python
        # Get singleton
        ctrl = Controller()

        # Get all ecoinvent activities associated with the key-word 'banana production'
        search_result = ctrl.search_ecoinvent_activities_name("banana production")

        # Choice of one of thoses activities (its position in the previously retrieved list)
        starting_act = random.randint(0, len(search_result))

        # Begin a new groupment of activities named "My banana production" from the chosen activity
        ctrl.validate_first_activity("My banana production", starting_act)

        # Get all the activities linked to the one already saved in the group
        linked_acts, _ = ctrl.find_linked_activities()

        # Choice of the position in the linked activities list of an other activity to add to the group
        second_act = random.randint(0, len(linked_acts))

        # Save the second activity to the group
        ctrl.add_new_activity(second_act)

        # Get all the activities linked to the two already saved in the group (except for already internal activities)
        linked_acts, _ = ctrl.find_linked_activities()

        # Choice of the position in the linked activities list of a third activity to add to the group
        third_act = random.randint(0, len(linked_acts))

        # Save the third activity to the group
        ctrl.add_new_activity(third_act)

        # Compile and save the group
        ctrl.save_group()

        # Initilize singleton for futur use
        ctrl.reinit()
        ```
    
    Args:
        modified_config (tuple, optional): tuple representing which
            element of the configuration file must be considered as
            the name of te database in which new groups of activities
            will be saved. The first element of the tuple is the config's
            section, the second is the parameter's name. Defaults to 
            ('SETUP', 'DATABASE_NAME').

    Attributes:
        conf (ConfigParser): parsed `config.ini `app file, containing all
            the configuration values needed for controller and model
        ecoinvent (SQLiteBackend): Brightway2 database's backend of 
            initial Ecoinvent database
        modified (SQLiteBackend): Brightway2 database's backend of 
            the database in which new groups of activities are saved
        group (Activity_group): Instance of the current activity group
            that the controller is building
        current_act_set (list): list of activities currently send to interface,
            in order to make a choice. It is expected from the interface to 
            give the choice as a number, representing the position of the activity 
            in this list
        current_exc_set (list): list of exchanges associated to the `current_act_set`
            attribute. This list is sorted the same way than the other one, so that
            an activity at a specific position in the `current_act_set` list is 
            associated with the exchange at the same position. Every activity of
            `current_act_set` have one unique exchange associated that links it to
            an activity that is already inside the group.
        actvity_list (list): list of activities already inside the group, sorted 
            in the order in which they have been selected. The difference with 
            the list of inside activities of the group is the order: in the 
            second case, they are shorted in the order of the internal relational 
            graph.
        current_distribution (list): list of integers representing, inside the
            `current_act_set`, the separation (position in the list) between activities 
            linked to the different already saved activities. It's equivalent to say 
            that thoses represent the positions in `current_exc_set` were the output of 
            the exchange switch. This list is ordonned the same way than 'actvity_list'. 
            This is useful for interface: it allows to show to the user which activity 
            is linked to which already selected activity
        checker (Checker): Checker instance used to test the current group of activity,
            to be sure that data is reliable
    """

    _instance = None

    def __new__(cls, modified_config=('SETUP', 'DATABASE_NAME')):
        """Function called when an instance of the Controller is created.
        If the instance does not already existes, this will create it, else
        this will return the already-created instance.

        Args:
            modified_config (tuple, optional): tuple representing which
            element of the configuration file must be considered as
            the name of te database in which new groups of activities
            will be saved. The first element of the tuple is the config's
            section, the second is the parameter's name. Defaults to 
            ('SETUP', 'DATABASE_NAME').

        Raises:
            RuntimeError: If project as not been setup at all (or the setup 
                in the coniguration file has been changed), eg. the `setup.py`
                file needs to be runned.

        Returns:
            Controller: singelton of Controller class
        """

        if cls._instance is None:
            cls._instance = object.__new__(cls)
            
            # Importing config.ini
            cls.conf = ConfigParser()
            cls.conf.read(os.path.join(os.path.dirname(__file__), "..", "..", "config.ini"))
            
            # Logger configuartion
            partial_log_file = check_config(cls.conf, "RUNTIME", "LOGGER", "output:runtime.log").split(':')
            full_log_file = path.join(*partial_log_file)
            form = '%(levelname)s : %(message)s'
            logging.basicConfig(filename=full_log_file, filemode='w', format=form, level=logging.INFO)
            console = logging.StreamHandler()
            console.setLevel(logging.WARNING)
            console.setFormatter(logging.Formatter(form))
            logging.getLogger().addHandler(console)

            if not ("SETUP" in cls.conf and "TEST" in cls.conf and "LOG" in cls.conf and "RUNTIME" in cls.conf):
                logging.error("Non-valid 'config.ini' file, please configure it with SETUP, TEST, LOG and RUNTIME sections")

            # Databases configuration
            project_name = check_config(cls.conf, "SETUP", "PROJECT_NAME", "isterre")
            if not project_name in bw2.projects:
                raise RuntimeError("Please setup project before using app!")
            bw2.projects.set_current(project_name)
            cls.ecoinvent = bw2.Database("ecoinvent")
            modified_name = check_config(cls.conf, *modified_config, "modified")
            cls.modified = bw2.Database(modified_name)
            cls._initial_modified = bw2.Database(modified_name)

            # Other elements
            cls.group = None
            cls.current_act_set = None
            cls.current_exc_set = None
            cls.current_distribution = None
            cls.actvity_list = []

            # For tests
            cls.checker = Checker(cls.conf)

        return cls._instance

    @classmethod
    def start_new_group(cls, name, activity, info=None):
        """Begin a new group of activities. Doesn't return anything
        but update `cls.group`.

        Args:
            name (str): Name of the new groupment
            activity (Activity): Main activity of new group (eg. final
                output activity of group)
            info (dict, optional): if not None, dictionary of meta-informations
                for the group. Defaults to None.
        """

        if cls.group:
            logging.warning("The group " + str(cls.group.get_name()) + " (uuid: " + str(cls.group.get_uuid()) + ") is overwrited to create new group.")
        cls.group = Activity_group(name, cls.modified, cls.conf, info=info)
        cls.group.add_activity(activity)
        cls.actvity_list.append(activity)

    @classmethod
    def search_ecoinvent_activities_name(cls, name_search):
        """Search a list of activitiesin the ecoinvent database
        given (a) specific(s) key-word(s).
        Returns the list and update `cls.current_act_set`

        Args:
            name_search (str): string of key-words (research to make)

        Returns:
            list: list of activities found in the database given the 
                research string
        """

        cls.current_act_set = cls.ecoinvent.search(name_search)
        return cls.current_act_set
    
    @classmethod
    def search_modified_activities_name(cls, name_search):
        """Search a list of activities in the database where new
        groups of activities are saved, given (a) specific(s) key-word(s).
        Returns the list and update `cls.current_act_set
`
        Args:
            name_search (str): string of key-words (research to make)

        Returns:
            list: list of activities found in the database given the 
                research string
        """

        cls.current_act_set = cls.modified.search(name_search)
        return cls.current_act_set
    
    @classmethod
    def random_ecoinvent_activity(cls):
        """Returns a random activity from the ecoinvent database

        Returns:
            Activity: a random Brightway2 activity from ecoinvent
        """

        return cls.ecoinvent.random()

    @classmethod
    def validate_first_activity(cls, name, pos, info=None):
        """From a position in `cls.current_act_set`, create 
        a new group with the associated activity, and a given
        name.

        Doesn't return anything but update `cls.group`, by 
        creating a new group, and if a group was already 
        set, overwrite it.

        Args:
            name (str): Name of the new group to begin
            pos (int): Position of group's main activity in 
                `cls.current_act_set`
            info (dict, optional): if set, dictionary of 
                meta-informations for the group. Defaults 
                to None.
        """

        cls.start_new_group(name, cls.get_activity_from_pos(pos), info)

    @classmethod
    def find_linked_activities(cls):
        """Find all the technosphere input of all the current 
        activities of the group (except for the ones that are 
        already in the group), and put them in `cls.current_act_set`,
        with their associated exchanges in `cls.current_exc_set`

        Update `cls.current_act_set`,`cls.current_exc_set` and 
        `cls.current_distribution`
        
        Returns:
            tuple: 2D-tuple containing
            
            * list: `cls.current_act_set`, corresponding to the list 
                of found activities
            * list: `cls.current_exc_set`, corresponding to the list 
                of exchanges between activities of `cls.current_act_set`
                and already selected activities for group
        """

        activities = cls.group.get_activities()
        activities_keys = [activity_key(a) for a in activities]
        cls.current_act_set = []
        cls.current_exc_set = []
        cls.current_distribution = []
        for act in activities:
            for tech in act.technosphere():
                key = exchange_input_key(tech)
                if key not in activities_keys:
                    cls.current_act_set.append(get_activity_by_key(key))
                    cls.current_exc_set.append(tech)
            cls.current_distribution.append(len(cls.current_act_set))

        return cls.current_act_set, cls.current_exc_set

    @classmethod
    def add_new_activity(cls, pos):
        """Add a new activity to the current group, given its
        position in `cls.current_act_set`.

        Args:
            pos (int): position of the activity to add to group
                inside the `cls.current_act_set` list

        Raises:
            RuntimeError: if the controller's group has not been 
                previously initilized
        """

        if not cls.group:
            raise RuntimeError("New group not initilized, please use Controller.start_new_group() before using Controller.add_new_activity()")
        
        if cls.group.verify_activity_exists(cls.get_activity_from_pos(pos)):
            cls.group.add_exchange(cls.get_exchange_from_pos(pos))
        else:
            cls.group.add_activity(cls.get_activity_from_pos(pos), cls.current_exc_set[int(pos)])
            cls.actvity_list.append(cls.get_activity_from_pos(pos))
    
    @classmethod
    def get_activity_from_pos(cls, pos):
        """Returns the corresponding activity
        for a specific position in `cls.current_act_set`.

        Args:
            pos (int): position in `cls.current_act_set`
                of the activity to get.

        Returns:
            Activity: Brightway2 activity corresponding
                to the given position
        """

        return cls.current_act_set[int(pos)]
    
    @classmethod
    def get_exchange_from_pos(cls, pos):
        """Returns the corresponding exchange
        for a specific position in `cls.current_exc_set`.

        Args:
            pos (int): position in `cls.current_exc_set`
                of the exchange to get.

        Returns:
            Exchange: Brightway2 exchange corresponding
                to the given position
        """

        return cls.current_exc_set[int(pos)]

    @classmethod
    def save_group(cls):
        """Compile and save current group to the database.
        To use when the group is finised to be setup.
        """

        cls.group.compile_and_save()
    
    @classmethod
    def remove_last_activity(cls):
        """Remove from group the last activity 
        added to the group. 
        
        Info:
            If the activity to remove is the main 
            activity, be careful because next added 
            activity must be main activiy and so 
            `cls.find_linked_activities` will returns 
            empty list (should use 
            `cls.search_ecoinvent_activities_name`)

        Returns:
            Activity: if an activity has been removed,
                returns the removed Brightway2 activity. 
                Else returns False
        """

        if cls.actvity_list:
            to_remove = cls.actvity_list[-1]
            cls.group.delete_activity(to_remove)
            cls.actvity_list.pop()
            return to_remove
        return False
    
    @classmethod
    def reinit(cls):
        """Initilize controller. Because Controller is 
        a singleton, every time a new group must be 
        setup, the interface should call this function 
        to put Controller in its initial configuration.
        """

        cls.modified = cls._initial_modified
        cls.group = None
        cls.current_act_set = None
        cls.current_exc_set = None
        cls.current_distribution = None
        cls.actvity_list = []

    @classmethod
    def save_and_test(cls, verbose=False, keep=False):
        """Compile, save and test the group, to be sure that
        given data is reliable.

        Args:
            verbose (bool, optional): if set to true, lots of 
                statistical informations will be displayed in
                standard output. Thoses are the testing informations.
                Colored informations mean a warning (yellow) or an
                anomaly (red). Defaults to False.
            keep (bool, optional): If set to False, the group will
                be removed from database after testing. This is useful
                for mass test the program. Be careful to set it to True 
                if the test is only a verification, and not an end in 
                itself. Defaults to False.
            
        Info:
            If errors are detected, it is recommanded to do a Monte 
            Carlo verification with thoses by calling 
            `cls.verify_errors_stability`, because error detction is 
            unstable due to bad condition number of technosphere 
            matrix.

        Returns:
            tuple: 2D-tuple containing

            * int: the level of error. 0 in case of test passed without 
                problem, 1 in case of warnings detected, 2 in case of 
                errors detected.
            * list: list of errors detected, or list warnings detected, 
                or None if everything went fine. Thoses lists contains
                3D-tuple (see `src.utils.tests.checker.check_level` 
                documentation to understand this data structure)
        """

        infos, absolute, relative, others = cls.group.compile_save_and_test(keep=keep)

        if verbose:
            write_title("Infos")
            write_dict(infos)
            write_title("Absolute errors stats")
            print_checked_dict(absolute, cls.checker.absolute)
            write_title("Relative errors stats")
            print_checked_dict(relative, cls.checker.relative)
            write_title("Others errors stats")
            print_checked_dict(others, cls.checker.other)
        else:
            w, e = check_level([absolute, relative, others], cls.checker.list)
            if len(e) > 0:
                return 2, e 
            elif len(w) > 0:
                return 1, w
            else:
                return 0, None

    @classmethod
    def verify_errors_stability(cls, errors):
        """Do a Monte Carlo verification on errors detected by 
        calling `cls.save_and_test`, to check if errors are recurrent
        or juste punctual anomalies due to LCA's algebra 
        instability.

        Args:
            errors (list): list of 3D-tuple representing errors.
                See `src.utils.tests.checker.check_level` for 
                more informations. Thoses are the errors which 
                the stability will be tested.

        Returns:
            bool: False if at least one error has been detected as
                recurrent, else True.
        """

        iterations = check_config(cls.conf, "TEST", "MONTE_CARLO", 30, int)
        for e in errors:
            if not cls.group.monte_carlo_test(e, cls.checker, iterations=iterations):
                return False 
        return True
    
    @classmethod
    def export_group_data(cls, flows_type, **kwargs):
        """Export the group data in a csv file (after compilation).
        The csv file will be named `<group name>.csv`.

        Args:
            flows_type (str): Type of flows to export, must be "technosphere",
                "biosphere"
            **kwargs: arguments of `src.utils.api_utils.flows_to_csv`,
                except for `iterator` and `filename`.

        Raises:
            RuntimeError: if group hasn't been previously compiled and saved
        """

        assert flows_type in ["technosphere", "biosphere"]

        if cls.group.compiled is None:
            raise RuntimeError("Data cannot be exported if group hasn't been previously compiled and saved")
        
        techno = flows_type == "technosphere"
        bio = flows_type == "biosphere"
        if bio:
            export_biosphere_flows(cls.group.compiled, filename=cls.group.name + "_bio.csv", **kwargs)
        elif techno:
            export_technosphere_flows(cls.group.compiled, filename=cls.group.name + "_tech.csv", **kwargs)
