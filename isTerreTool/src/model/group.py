import numpy as np
import pandas as pd
import logging
import progressbar
from uuid import uuid4
from os import makedirs, path
from time import time
from ..utils.dataobj import *
from ..utils.tests import *

class Activity_group:
    """Class representing a set of linked activities that have to be grouped.

    It incudes:

    * Set of activities to group.
    * Metadata for the grouped activity.
    * Methods to compile the set of activities to an unique grouped activity.
    * Methods to test if the grouped data is reliable.

    The algorithm used to group activities is the Leontief inverse matrix.

    Attributes:
        name (str): Name of the grouped activity.
        info (dict): Dictionary representing optional metadata of Brightway2
            Activity, for the grouped activity.
        database (Database): Brightway2 Database object in which the grouped 
            activity will be saved.
        uuid (UUID): Unique identifier for grouped activity.
        compiled (Activity): Brightway2 Activity object representing the grouped
            activity.
        activities (list): List of Brightway2 activities corresponding to the 
            activities to group. Thoses are sorted in consequential order. The 
            last element of the list is the terminal activity of groupment.
        exchanges (list): List of Brightway2 Exchanges, which represents the links
            between activities to group (eg `activities` attribute).
        final_activity (Activity): Brightway2 Activity which will be the terminal
            activity of the groupment. Must be the same as the last activity of
            `self.activities` list.
        log (bool): If tests and information methods have to save data in external
            log files.
        config (ConfigParser): Parsed `config.ini `app file, containing all the 
            configuration values needed for controller and model.
    """

    def __init__(self, name, database, config, info=None):
        """Initilize a set of activities to group.

        Args:
            name (str): Name to give to the grouped activity.
            database (Database): Brightway2 Database in which the grouped activity
                will be saved.
            config (ConfigParser): Parsed `config.ini `app file, containing all the 
                configuration values needed for controller and model.
            info (dict, optional): Dictionary containing optional metadata for the
                grouped activity. Defaults to None.
        """

        self.name = name
        if not info:
            self.info = dict()
        else:
            self.info = info
        self.database = database
        self.uuid = uuid4() # Careful! In brightway2, they use an MD5 hash of a set of parameters, see https://2.docs.brightway.dev/intro.html#activity-hashes
        self.compiled = None
        self.activities = []
        self.exchanges = []
        self.final_activity = None
        self.log = check_config(config, "LOG", "LOG", True, bool)
        self.config = config
        self._cond_verification = True

    def add_activity(self, activity, exchange=None):
        """Add an activity to the set of activities to group. This activity have to
        be linked to another activity already selected for the groupment (eg inside
        the set), exept if it is the first activity added to the group, corresponding
        to the terminal activity.

        In other words, add the `activity` argument to `self.activities` and the 
        `exchange` argument to `self.exchanges`, after verification of the validity 
        of given arguments.

        Args:
            activity (Activity): Brightway2 Activity to add to the set.
            exchange (Exchange, optional): Needs to be given if the activity to add
                isn't the first activity of the groupment: the first activity needs
                to be the terminal activity of the groupment. It corresponds to the 
                link between the activity to add to the set, and an activity which
                is already inside the set. Defaults to None.

        Raises:
            RuntimeError: If the `exchange` isn't given and the set of activities
                isn't empty. The `exchange` argument have to be given if the activity
                to add isn't the first activity of the group.
            RuntimeError: If the `exchange` doesn't link the new activity to an 
                activity already registred in the set.
            RuntimeError: If the `exchange` isn't linked to the given activity.

        Returns:
            list: List of Brightway2 Activities of the group up to date. Corresponding
                to `self.activities` after update.
        """

        if self.activities:
            if not exchange:
                raise RuntimeError("Activity already initialized but no exchange given. Please explicit exchange between new activity and already validated activities.")
            
            if code := self.verify_activity_exists(exchange_input_key(exchange)):
                given_activity_key = exchange_output_key(exchange)
                gap = -1
            elif code := self.verify_activity_exists(exchange_output_key(exchange)):
                given_activity_key = exchange_input_key(exchange)
                gap = 0
            else:
                raise RuntimeError("Non-valid exchange: the other activity isn't in the already validated activities.")

            if activity_key(activity) != given_activity_key:
                raise RuntimeError("The activity needs to be at one of the end of the exchange...")
                
            self.activities.insert(code + gap, activity)
            self.exchanges.append(exchange)

        else:
            self.activities.append(activity)
            self.final_activity = activity
        
        return self.activities
    
    def add_exchange(self, exchange):
        """Add a link between activities of the group. Update `self.exchanges`
        if the given `exchange` is valid for the current group.

        This function is quite depreciated, because unliked internal exchange
        are automatically added to the group during compilation of the grouped
        activity.

        Args:
            exchange (Exchange): Brightway2 Exchange to add to the group.

        Raises:
            RuntimeError: If at least one of the two activities linked by the 
                `exchange` is not in the current set of activities of the group.
                The `exchange` is then not valid.

        Returns:
            list: List of Brightway2 Exchanges internal to the group up to date.
                Corresponding to `self.exchanges` after update.
        """

        input_key = exchange_input_key(exchange)
        output_key = exchange_output_key(exchange)
        if verify_activity_exists(input_key) and verify_activity_exists(output_key):
            self.exchanges.append(exchange)
        else:
            raise RuntimeError("Input or output activity of exchange isn't already given, please use 'Activity_group.add_activity()' function")
        return self.exchanges

    def delete_activity(self, activity):
        """Remove activity from the group. It is imposible to remove an activity possessing
        other activities of the group as input. This would break the production line 
        and invalidate the group.

        Args:
            activity (Activity): Brightway2 Activity of `self.activities` to delete.

        Raises:
            RuntimeError: If the given activity is a dependency for other activities
                in the group, eg some other activities of the group are inputs of the
                given activity. You must delete thoses inputs before removing the given
                activity.

        Returns:
            bool: True if the given activity has been removed as expected. False if
                the given activity wasn't already in `self.activities`, and so the
                function ignored it.
        """

        if activity_key(activity) in [exchange_output_key(e) for e in self.exchanges]:
            raise RuntimeError("Cannot remove activity which is at the output node of an exchange...")
        
        if activity in self.activities:
            self.activities.remove(activity)
            for i, exchange in enumerate(self.exchanges):
                if exchange_input_key(exchange) == activity_key(activity):
                    self.exchanges.pop(i)
            return True
        
        if not len(self.activities):
            self.final_activity = None

        return False

    def compile_and_save(self):
        """Compile the current set of activities of the group to an unique 
        grouped activity, and saves it in the database.

        The compiled grouped activity will have:

        * `self.uuid` as uuid.
        * `self.name` as name.
        * Metadata:
            * `self.info` elements.
            * The terminal activity at `"main activity"` key.
            * `self.activities` at `"inside activities"` key.
            * `self.exchanges` at `"inside exchange"` key.
            * The list of internal activities and exchanges at the bottom of 
                the `comment` section.
            * If `reference product`, `unit` or `production amount` aren't
                given in `self.info`, then the value of thoses empty fields
                will be the same as the ones of the terminal activity.

        During the process, all links between internal activities which are not 
        listed in `self.exchanges` will be found and added to the `self.exchanges`
        list. 

        The database in which the new activity will be save is `self.database`.

        The value of `self.compiled` will be updated with the compiled grouped
        activity.

        Raises:
            RuntimeError: If some of the activities in `self.activities` are not
                linked with at least one other activity of the groupe. Should not
                append if activities have been added with the `self.add_activity`
                method.

        Returns:
            Activity: Brightway2 Activity corresponding to the compiled grouped
                activity. Corresponding to `self.compiled`.
        """
    
        if not self.check_consistency():
            raise RuntimeError("Activities of the group are not linked one with the others... Imposible to compile...")

        new_activity = self.database.new_activity(self.uuid)
        new_activity['name'] = self.name

        self.info['main activity'] = self.final_activity
        self.info['inside activities'] = self.activities
        self.info['inside exchanges'] = self.exchanges
        
        if 'comment' in self.info:
            self.info['comment'] += "\n-------------------\nInternal activities:\n"
        else:
            self.info['comment'] = "-------------------\nInternal activities:\n"

        new_activity.save()
        
        # First loop to get all internal exchanges
        for act in self.activities:
            for ex in act.exchanges():
                if ex['type'] == "biosphere":
                    continue
                if exchange_input_key(ex) == exchange_output_key(ex) and abs(ex.amount) == 1 and ex['type'] == 'production':
                    continue
                if exchange_links(ex, self.activities) and not self._is_exchange_in_set(ex):
                    logging.info("Ignored internal exchange: " + str(ex))
                    self.exchanges.append(ex)
            self.info['comment'] += str(act) + ": " + act.key[KEY_DATABASE] + ", " + act.key[KEY_CODE] + "\n"

        L, E, extern = self._build_leon_extern_matrix()
        act_array = L[:, 0]

        for i, e in enumerate(extern):
            c = self.activities.index(e.output)
            exchange_amount = act_array.dot(E[i, :])
            new_activity.new_exchange(input=e['input'], amount=exchange_amount, type=e['type']).save()


        self.info['comment'] += "-------------------\nInternal exchanges:\n"
        for e in self.exchanges:
            self.info['comment'] += str(e) + "\n"

        if "reference product" in self.info:
            if not "unit" in self.info:
                self.info["unit"] = self.final_activity["unit"]
            if not "production amount" in self.info:
                self.info["production amount"] = self.final_activity["production amount"]
        else:
            self.info["reference product"] = self.final_activity["reference product"]
            self.info["unit"] = self.final_activity["unit"]
            self.info["production amount"] = self.final_activity["production amount"]

        new_activity.new_exchange(input=new_activity, amount=self.info["production amount"], type="production")

        if self.info:
            for k in self.info:
                new_activity[k] = self.info[k]
        
        new_activity.save()
        self.compiled = new_activity

        return self.compiled
    
    def test(self, other=None, to_test=None):
        """Create statistics to identify if compiled activity is reliable. Thoses statistics
        will have then to be verified with the `Checker` class.

        The test system works as follows:

        * An LCI is done for the grouped activity.
        * An LCI is done for the terminal activity.
        * Some statistics are generated by comparing the two vectors.
        * Thoses statistics have then to be checked by the `Checker` class (not done by this
            method).

        More informations ontest system are given in official documentation. See project 
        README for instance.

        The statistics are divided into 3 groups:

        * Statistics on the vector of term to term absolute errors:
            * Mean
            * Median
            * 1st quantile
            * 3rd quantile
            * Variance
            * Maximum
            * Minimum
            * Maximum position
            * Minimum position
            * Benford p-value
        * Statistics on the vector of term to term relative errors:
            * Mean
            * Median
            * 1st quantile
            * 3rd quantile
            * Variance
            * Maximum
            * Minimum
            * Maximum position
            * Minimum position
            * Benford p-value
        * Other statistics:
            * LCI time before groupment
            * LCI time after groupment
            * Found function of ref (if `self.log`, give the path to graph's image)
            * Any other stats given in the `other` dict in argument


        Args:
            other (dict, optional): Dict of any statistics that are not made by this method.
                Especially useful if `self.log` is set to true, and some informations must
                be saved in addition of the default ones. This dict will be modified by adding
                new informations before beeing returned. Defaults to None.
            to_test (Activity, optional): The activity to test. Useful only if the activity
                to test is not `self.compiled`. Should not be used, except to test a copy of
                `self.compiled` to do a Monte Carlo verification. Defaults to None.

        Raises:
            RuntimeError: If the `self.test()` function has been used before compiling the 
                groupe, eg `self.compiled` is None.

        Returns:
            dict: Test's statistics on the vector of absolute errors.
            dict: Test's statistics on the vector of relative errors.
            dict: Test's other statistics. Corresponding to `other` argument with some more
                informations.
        """

        if not self.compiled:
            raise RuntimeError("Please use 'Activity_group.compile_and_save()' method before using Activity_group.test().")
        
        if self.log:
            log_dir = check_config(self.config, "LOG", "LOG_DIRECTORY", "output:log").split(':')
            dir_path = path.join(path.dirname(__file__), "..", "..", *log_dir, self.name)
            if not path.isdir(dir_path):
                makedirs(dir_path)
        
        if not other:
            other = dict()
        
        if to_test is None:
            to_test = self.compiled

        valid_relative_error_cutoff = check_config(self.config, "TEST", "VALID_RELATIVE_ERROR_CUTOFF", 1e10-9, float)

        tests = dict()

        lca_grp = bw2.LCA({to_test: 1})
        lca_ref = bw2.LCA({self.final_activity: 1})

        t = time()
        lca_grp.lci()
        other["LCI time after groupment"] = time() - t
        t = time()
        lca_ref.lci()
        other["LCI time before groupment"] = time() - t

        comp = construct_comparator(lca_ref, lca_grp)

        errors = [abs(a*self.final_activity["production amount"] - b) for a, b in comp]
        singularity = [(i, a, g) for i, (a, g) in enumerate(comp) if not bool(a) == bool(g) and (a > valid_relative_error_cutoff or g > valid_relative_error_cutoff)]
        relative_error = [e / abs(a) for e, (a, g) in zip(errors, comp) if bool(a) == bool(g) and (a > valid_relative_error_cutoff or g > valid_relative_error_cutoff)]
        relative_error_loc = [i for i, (a, g) in enumerate(comp) if bool(a) == bool(g) and (a > valid_relative_error_cutoff or g > valid_relative_error_cutoff)]
        other["Singularity"] = len(singularity)

        if self.log:
            save_format = check_config(self.config, "LOG", "GRAPH_FORMAT", "png")
            other["Found function of ref"] = graph_ref_found(comp, path.join(dir_path, "found_function_of_ref." + save_format), title=self.name)
            errors_stats = get_statistics_from_vector(errors, save_path=path.join(dir_path, "benford_absolute." + save_format), name=self.name + " absolute errors")
            relative_stats = get_statistics_from_vector(relative_error, save_path=path.join(dir_path, "benford_relative." + save_format), name=self.name + " relative errors")
            with open(path.join(dir_path, "stats.log"), 'w') as f:
                write_title("Absolute errors stats", file=f)
                write_dict(errors_stats, file=f)
                
                write_title("Relative errors stats", file=f)
                write_dict(relative_stats, file=f)

                if other:
                    write_title("Others", file=f)
                    write_dict(other, file=f)
            with open(path.join(dir_path, "absolute.data"), 'w') as f:
                for e in errors:
                    f.write(str(e) + '\n')
            if singularity:
                with open(path.join(dir_path, "singularity.data"), 'w') as f:
                    for s in singularity:
                        f.write(str(s) + '\n')
            with open(path.join(dir_path, "relative.data"), 'w') as f:
                for r, l in zip(relative_error, relative_error_loc):
                    f.write(str(r) + ";" + str(l) + '\n')
        else:
            errors_stats = get_statistics_from_vector(errors)
            relative_stats = get_statistics_from_vector(relative_error)
        
        return errors_stats, relative_stats, other
        
    def compile_save_and_test(self, keep=False):
        """Compile the group of activities into one grouped activity, save it in 
        `self.database` database, and returns statistics to test the reiability
        of the groupment.

        See `self.test()` technical documentation for more informations about the
        statistics dictionnaries.

        See `self.info()` technical documentation for more informations about the
        informations dictionary.

        Args:
            keep (bool, optional): If set to False, the compiled activity will be
                removed from the database after getting test's statistics. This 
                is useful for automatic testing, to not keep in Brightway2 project
                useless data. Please set it to True if you want to test real data, 
                which will then be used. Defaults to False.

        Returns:
            dict: Dictionnary of informations about the group of activities.
            dict: Test's statistics on the vector of absolute errors.
            dict: Test's statistics on the vector of relative errors.
            dict: Test's other statistics. Corresponding to `other` argument with some more
                informations.
        """

        timing = dict()
        t = time()
        self.compile_and_save()
        timing["Compilation time"] = time() - t
        infos = self.analyse_infos()
        stats_abs, stats_rel, other = self.test(other=timing)

        if not keep:
            self.compiled.delete()

        return infos, stats_abs, stats_rel, other

    def save(self):
        """If grouped activity has already been compiled, save it again. Else, compile the
        group of activities and save it.
        """

        if not self.compiled:
            logging.warning("'Activity_group.compile_and_save()' method should be used before using Activity_group.save().")
            self.compile_and_save()
        self.compiled.save()

    def check_consistency(self):
        """Verify that the activities of the group are indeed linked the ones with the others.
        It assures that no problem append during the process of adding activities to the group.

        Returns:
            bool: True if all activities in `self.activities` are linked with the exchanges in
                `self.exchanges`. Else, False.
        """

        if len(self.activities) == 1 and len(self.exchanges) == 0:
            return True
        for a in self.activities:
            if not activity_key(a) in [exchange_input_key(e) for e in self.exchanges] and not activity_key(a) in [exchange_output_key(e) for e in self.exchanges]:
                return False
        return True

    def verify_activity_exists(self, activity_key):
        """Check if an activity is already in `self.activities`. If it is the
        case, returns the position of the activity in `self.activities` + 1, else
        return False. This allows to use this function as a boolean function.

        Args:
            activity_key (tuple): Tuple representing the key of a Brightway2 
                Activity, eg the uuid of the activity and its database. Corresponding
                to Activity.key.

        Returns:
            int, bool: Position of the activity in `self.activities` incremented by 1,
                or False if the activity is not in `self.activities`.
        """

        for i, act in enumerate(self.activities):
            if act['code'] == activity_key[KEY_CODE] and act['database'] == activity_key[KEY_DATABASE]:
                return i + 1 # the return value of the function is always > 0 if activity found, so it can be used as a boolean function
        return False

    def add_info(self, info):
        """Add metadata for the grouped activity. Eg, update `self.info`.

        This method take in input a dictionary, and combine it with the current 
        `self.info`. There are then two possibilities for each key in the given
        dictionary:

        * The key is already in `self.info`, and the corresponding value will be
            overwrited in `self.info` by the new value.
        * The key is not yet in `self.info` and is then added to `self.info`, with its 
            corresponding value.

        Args:
            info (dict): Dictionary of new and/or updated metadata.
        """

        if self.compiled:
            for i in info:
                self.compiled[i] = info[i]
            self.save()
        else:
            for i in info:
                self.info[i] = info[i]

    def analyse_infos(self):
        """Analyse the structure of the group of activities, and returns a dictionary
        containing some informations about it. Can be useful for instance to understand
        a potential instability of the group, or for debugging after a faild test.

        If `self.log` is set to True, this will save all this datat in a text file called
        `info.log`, in the log directory defined in the configuration file.

        The informations of the returned dictionary are the following:

        * intern activities: Number of activities in the group.
        * biosphere flows: Total number of biosphere flows interacting with the activities
            of the group. This is the same as the number of biosphere flows of the grouped
            activity.
        * extern techno: Total number of technosphere flows interacting with the activities
            of the group, at the exception of the internal links between the activities of
            the group. This is the same as the number of technosphere flows in input of the
            grouped activity.
        * extern prod: Total number of production exchanges interacting with the activities
            of the group, at the exception of the internal and unitary production exchanges.
        * extern sub: Total number of substitution exchanges interacting with the activities
            of the group, at the exception of the internal substitution exchanges.
        * intern techno: Number of technosphere flows linking two activities of the group.
        * intern prod: Number of production exchanges internal of the group.
        * intern sub: Number of substitution exchanges internal of the group.
        * intern negative: Number of negative exchanges internal of the group.
        * treatement act: Number of activities of treatement (negative production amount) in
            the group.
        * production act: Number of activities of production (positive production amount) in
            the group.
        * undeclared links: Number of links between two activities of the group which are not
            in the `self.exchanges` list.
        * cycle: True if there is an cycle inside the group.
        * multiple input: True if at least an activity of the group gets multiple inputs of
            activities of the group.
        * graph representation: Link to a graph visualization of the group of activities and
            the links between them.

        Returns:
            dict: Dictionary containing all the informations about the group listed above.
        """

        if self.log:
            log_dir = check_config(self.config, "LOG", "LOG_DIRECTORY", "output:log").split(':')
            dir_path = path.join(path.dirname(__file__), "..", "..", *log_dir, self.name)
            if not path.isdir(dir_path):
                makedirs(dir_path)

        infos = dict()
        infos['intern activities'] = len(self.activities)
        infos['biosphere flows'] = 0
        infos['extern techno'] = 0
        infos['extern prod'] = 0
        infos['extern sub'] = 0
        infos['intern techno'] = 0
        infos['intern prod'] = 0
        infos['intern sub'] = 0
        infos['intern negative'] = 0
        infos['treatement act'] = 0
        infos['production act'] = 0
        infos['undeclared links'] = 0
        all_intern_exch = []
        for act in self.activities:
            if act["production amount"] == 1:
                infos['production act'] += 1
            elif act["production amount"] == -1:
                infos['treatement act'] += 1
            for ex in act.exchanges():
                if ex['type'] == "biosphere":
                    infos['biosphere flows'] += 1
                    continue
                if exchange_links(ex, self.activities):
                    if not self._is_exchange_in_set(ex) and ex['type'] != "production" :
                        infos['undeclared links'] += 1
                    all_intern_exch.append(ex)
                    if ex.amount < 0:
                        infos['intern negative'] += 1
                    if ex['type'] == "technosphere":
                        infos['intern techno'] += 1
                    elif ex['type'] == "production":
                        infos['intern prod'] += 1
                    elif ex['type'] == "substitution":
                        infos['intern sub'] += 1
                else:
                    if ex['type'] == "technosphere":
                        infos['extern techno'] += 1
                    elif ex['type'] == "production":
                        infos['extern prod'] += 1
                    elif ex['type'] == "substitution":
                        infos['extern sub'] = 0
        

        graph = get_graph(all_intern_exch, name=self.name)

        cycles = [c for c in nx.simple_cycles(graph)]
        infos['cycle'] = len(cycles) != 0

        infos['multiple input'] = detect_multiple_input(all_intern_exch)

        if self.log:
            save_format = check_config(self.config, "LOG", "GRAPH_FORMAT", "png")
            output_name = check_config(self.config, "LOG", "OUTPUT_NAME", "output")
            infos['graph representation'] = save_graph(graph, dir_path, img_format=save_format, output_activity=self.final_activity, output_name=output_name)
            with open(path.join(dir_path, "infos.log"), 'w') as f:
                write_dict(infos, file=f)

        return infos

    def get_name(self):
        """Getter for the name of the grouped activity.

        Returns:
            str: Name of the grouped activity.
        """

        return self.name
    
    def get_uuid(self):
        """Getter for the uuid of the grouped activity.

        Returns:
            UUID: UUID of the grouped activity.
        """

        return self.uuid
    
    def get_activities(self):
        """Getter for the list of activities of the group.

        Returns:
            list: List of activities of the group.
        """

        return self.activities
    
    def make_copy_and_test(self):
        """Make a copy of the compiled activity and gets statistics of test for this copy given
        by `self.test`. Useful to test multiple time the same grouped activity.

        More precisions about test's statistics can be found at the `self.test()` function's
        documentation.

        Raises:
            RuntimeError: If the method has been used before the compilation of the grouped
                activity. `self.compile_and_save()` or `self.compile_save_and_test()` have to
                be called before this function.

        Returns:
            Activity: The copied activity.
            dict: Test's statistics on the vector of absolute errors.
            dict: Test's statistics on the vector of relative errors.
            dict: Test's other statistics. Corresponding to `other` argument with some more
                informations.
        """
        
        if not self.compiled:
            raise RuntimeError("Please use 'Activity_group.compile_and_save()' method before using Activity_group.make_copy().")

        copy = self.compiled.copy()
        copy.save()

        return copy, *self.test(to_test=copy)
    
    def multiple_tests_mode_in(self):
        """Fuction that setup multi-testing mode. Have to be called before making a Monte Carlo
        testing verification.
        """

        self.log = False 
        self._cond_verification = False
    
    def multiple_tests_mode_out(self):
        """Fuction that exit multi-testing mode. Have to be called after making a Monte Carlo
        testing verification.
        """

        self.log = check_config(self.config, "LOG", "LOG", True, bool)
        self._cond_verification = False

    def monte_carlo_test(self, to_check, checker, iterations=30):
        """Test multiple time the grouped activity, make a mean of the statistics obtained,
        and verify this mean with the checker.

        Info:
            This method does not fully respects the MVC architecture for two reasons: It
            prints a progress bar in the standard output, and it checks the statistics, 
            whereas this operation should be made by the controller (it is what's done for
            single testing).

            Dealing with this problem should be something to do for futures versions.

        Args:
            to_check (list): list of 2D-tuples containing the informations about which
                test statistics have to be checked multiple time. Every tuple represents
                a specific staistic. The first element of the tuple is the position of the
                dictionary of statistics in `Checker.list`, and the second element of
                the tuple is the key name of the statistic in the dictionary.
            checker (Checker): Checker instance used to verify if the tests' statistics 
                are respecting the thresholds of validity. It is the one that say if the
                test passed or not.
            iterations (int, optional): Number of tests that have to be done to consider
                the mean of the statistics representative. Defaults to 30.

        Returns:
            bool: True if the multiple testing verification passed. False else.
        """
        
        # Initilize
        database_name = check_config(self.config, "TEST", "DATABASE_TEST", "tests")
        if database_name in bw2.databases:
            db_there = True
            tests_database = bw2.Database(database_name)
        else:
            db_there = False
            tests_database = bw2.Database(database_name)
            tests_database.write({})
        self.multiple_tests_mode_in()

        # Change database
        saved_database = self.database
        self.database = tests_database

        # Progress bar
        progress = progressbar.progressbar(range(0, iterations))
        next(progress)

        # Tests
        values = []
        _, *stats = self.compile_save_and_test(keep=True) # First element is info
        activities = [self.compiled]
        values.append(stats[to_check[0]][to_check[1]])
        for _ in progress:
            c_act, *stats = self.make_copy_and_test()
            activities.append(c_act)
            values.append(stats[to_check[0]][to_check[1]])
        
        # Reinit
        self.database = saved_database
        if not db_there:
            del bw2.databases[tests_database.name]
        else:
            for a in activities:
                a.delete()

        # Verify result
        check_func = checker.list[to_check[0]][to_check[1]][Checker.TEST_POSITION]
        check_mean = checker.list[to_check[0]][to_check[1]][Checker.MEAN_POSITION]
        
        m = check_mean(values)

        self.multiple_tests_mode_out()

        if check_func(m) == 0:
            return True
        
        logging.error("Monte Carlo verification didn't end well. Value found: " + str(m) + " for test " + STATS_NAMES[to_check[0]] + ", " + to_check[1])
        return False
    
    def _is_exchange_in_set(self, exchange):
        """Verify if a given Brightway2 Exchange is in `self.exchanges` or not.

        Args:
            exchange (Exchange): Brightway2 Exchange that we want to check the presence in `self.exchanges`.

        Returns:
            bool: True if the exchange is in `self.exchanges`, else False.
        """

        key_exchanges = [(exchange_input_key(e), exchange_output_key(e), e.amount) for e in self.exchanges]
        if (exchange_input_key(exchange), exchange_output_key(exchange), exchange.amount) in key_exchanges:
            return True 
        return False
    
    def _build_leon_extern_matrix(self):
        """Build the Leontief inverse matrix of the group, and the matrix
        representing flows from the outside to the inside of the group.

        This function can be seen as a partial ICV, where the biopshere
        flows are the flows from the outside to the inside of the group, and
        the technosphere matrix is the reduction of the database's technosphere
        matrix to the activities of the group.

        Raises:
            RuntimeError: If an internal exchange's type does not respect the "production",
                "substitution" and "technosphere" Brightway2's exchanges classification.

        Returns:
            array: Leontief inverse matrix internal to the group.
            array: Extern-Intern matrix of the group.
            list: List of Extern-Intern exchanges.
        """

        n = len(self.activities)
        A = np.zeros((n, n))
        for e in self.exchanges:
            if e["type"] in ["production", "substitution"]:
                factor = -1
            elif e["type"] in ["technosphere"]:
                factor = 1
            else:
                raise RuntimeError("Invalid exchange type")
            c = self.activities.index(e.output)
            l = self.activities.index(e.input)
            if e.amount > 0:
                A[l, c] += e.amount * factor
            else:
                A[l, c] += -1 * e.amount * factor

        extern = []
        for a in self.activities:
            for e in a.exchanges():
                if not self._is_exchange_in_set(e):
                    if exchange_input_key(e) == exchange_output_key(e):
                        continue
                    extern.append(e)

        m = len(extern)
        E = np.zeros((m, n))

        for l, e in enumerate(extern):
            c = self.activities.index(e.output)
            E[l, c] = e.amount
        
        I = np.identity(n)

        techno = I - A
        
        if self._cond_verification:
            cond_limit = check_config(self.config, "RUNTIME", "MIN_COND_ALERT", 1000, int)
            cond = np.linalg.cond(techno)
            if cond > cond_limit:
                logging.warning("Unbalanced group, condition number of intern matrix equals to " + str(cond) + ". Might create some errors in result...")
        
        L = np.linalg.inv(techno)

        return L, E, extern
