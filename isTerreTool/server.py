#!/usr/bin/env python3

import os
import argparse as ap
from src.interface.command_line import command_line_interface, get_biosphere_flows

def start_server():
    """Start command-line interface with the specificities indicated by command-
    line arguments and `config.ini` variables.
    """

    args = setup_parser()
    if args.biosphere:
        get_biosphere_flows()
        return
    if args.test:
        # For use of pygraphviz on windows
        os.environ['KMP_DUPLICATE_LIB_OK'] = 'True'
    command_line_interface(export_bio=args.flows, export_tech=args.production, test=args.test)

def setup_parser():
    """Initilize and parse parser of command-line argument

    Command line arguments available with this function:

    * -b: Choose an already grouped activity, and export its biosphere flows in
        a csv file.
    * -p: At the end of groupment, export technosphere flows of grouped activity
        in a csv file.
    * -f:  At the end of groupment, export biosphere flows of grouped activity
        in a csv file.
    * -t:  At the end of groupment, test grouped activity to verify if its data
        is reliable.

    Returns:
        ArgumentParser: Parsed argparse.ArgumentParser instance.
    """
    
    parser = ap.ArgumentParser()
    parser.add_argument('-b', '--biosphere', help='Export biosphere flows of a grouped activity', action='store_true')
    parser.add_argument('-p', '--production', help='Export technosphere flows at the end of groupment', action='store_true')
    parser.add_argument('-f', '--flows', help='Export biosphere flows at the end of groupment', action='store_true')
    parser.add_argument('-t', '--test', help='Test that grouped data has been compiled without errors. See README for more informations', action='store_true')
    args = parser.parse_args()
    return args

def main():
    """Entry point for app.
    """

    start_server()

if __name__ == "__main__":
    main()
