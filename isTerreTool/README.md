# Mater Groupment Tool

*Author:* Baptiste de GOËR

*Year:* 2022

*Language:* Python 3.8 or higher

*Tested with:* Ecoinvent 3.4 cutoff

*README markdown*: GitLab Flavored Markdown

*Additional informations (fr)*: https://hal.inria.fr/hal-03736623

# Contents

[[_TOC_]]

# Goal
The global goal is to adapt ecoinvent data to [MATER model](https://hal.archives-ouvertes.fr/hal-03426225/document), modelization developed by the team of Olivier Vidal, at the ISTerre laboratory.

More specificly, this program, for now, allows the user to group a production line in the LCA database [Ecoinvent](https://ecoinvent.org/). He/she can then export biosphere or technosphere inputs of the grouped production line, to incorporate them as data into an other model, like MATER.

# Quick start

If you want to use this program, you will need an Ecoinvent dataset. The program has been tested with `Ecoinvent 3.4 cutoff`.

You will also need `python` 3.8 or higher (if you are on Windows, this will be installed at the same time than `miniconda`, if you are on Linux just be sure that your python version is at least the 3.8).

To use this program, you need to follow this steps:

* [Install miniconda](https://docs.conda.io/en/latest/miniconda.html)
* Clone the git: `git clone https://gitlab.inria.fr/steep/lcatools`
* Go into the `isTerreTool` folder
* Install requirements with conda : `conda install --file requirements.txt`
* Change the `ECOINVENT` variable in the `config.ini` file
* Initilize the program by usinge the `setup.py` script
* Run the program: `server.py`

## Quick start on Windows

### Install miniconda

[Link to the official documentation](https://docs.conda.io/projects/conda/en/latest/user-guide/install/windows.html)

To install miniconda on Windows, you need to [downald the Windows installer](https://docs.conda.io/en/latest/miniconda.html#windows-installers). Choose one with `python 3.8` or higher.

Then execute it, and accept the default installation settings.

Open the start menu, and search for `Anaconda Powershell Prompt (Miniconda3)`.

You will use this prompt everytime you want to use the APP, so it is recommended to create a shortcut on your desktop or in your taskbar, to easly open it.

Open the powershell, then type in it:

```
$ conda init
```

Close the powershell.

### Install git

If [git](https://git-scm.com/) is not yet installed in your computer, please install it.

You have 2 ways to install git.

#### 1rst possibility

Open `Anaconda Powershell Prompt`.

Type in the prompt:

```
$ conda install -c anaconda git
```

**Info:**

If you want to use git a little more than just to clone the repository, you might want to install it also in your `iTT` environment. If so, after creating the environment (see following instructions), you can redo the prevous command line after activating the `iTT` environment.

#### 2nd possibility

Download standelone [git installer](https://git-scm.com/download/win), and execute the installer.

Accept the default installation settings.


### Clone the git

Open the `Anaconda Powershell Prompt`.

Clone git by typing in the powershell:

```
$ git clone https://gitlab.inria.fr/steep/lcatools
```

Go into the isTerreTool folder:

```
$ cd lcatools\isTerreTool
```

### Create conda environnment and install requirements

In the current opened powershell, type:

```
$ conda create -n iTT -c conda-forge -c cmutel brightway2 jupyterlab
$ conda activate iTT
$ conda install pywin32
$ conda install --file requirements.txt
```

Close the powershell.

### Setup project

Change the `config.ini` file by giving to the `ECOINVENT` variable the path to the Ecospoild2 datasets of your Ecoinvent database.

Be careful, one windows, path are given with `\`, unlike Linux where path are given with `/`. 

It is recomended to open your datasets in the file explorer, and copy path in the navigation bar. Then copy it at the `ECOINVENT` variable of `config.ini` file.

Then, open the `Anaconda Powershell Prompt` and type in it:

```
$ conda activate iTT
$ python setup.py
```

Close the powershell.

### Use program

Now you should have everything in place.

Every time you want to use the program, you need to open the `Anaconda Powershell Prompt`.

Then go into the isTerreTool folder:

```
$ cd lcatools\isTerreTool
```

Acivate your conda environment:

```
$ conda activate iTT
```

Run the `server.py` script. It is recommended to use it with the `-t|--test` option:

```
$ python server.py -t
```

## Install Activity-Browser

Activity-Browser is a graphical interface that can be useful to visualize grouped data. You can install it using `miniconda`.

Open the `Anaconda Powershell Prompt` and type:

```
$ cd lcatools\isTerreTool
$ conda activate iTT
$ conda install activity-browser
```

You have then juste to activate the `iTT` environment and type `activity-browser` in the terminal to open Activity-Browser.

## Quick start on Linux

### Install miniconda

[Link to the official documentation](https://docs.conda.io/projects/conda/en/latest/user-guide/install/linux.html)

To install miniconda on Linux, you need to [downald the installation script](https://docs.conda.io/en/latest/miniconda.html#linux-installers).

Open then your favorite terminal, and run the downloaded script: 

```
$ sh Miniconda3-<version>.sh
```

Accept default configuration.

Close your terminal.

### Clone the git

Open your favorite terminal.

Clone the git by typing:

```
$ git clone https://gitlab.inria.fr/steep/lcatools
```

Go into the isTerreTool folder:

```
$ cd lcatools/isTerreTool
```

### Create conda environnment and install requirements

[Official documentation for conda evironments](https://docs.conda.io/projects/conda/en/latest/user-guide/tasks/manage-environments.html)

Using your current terminal, type the following commands:

```
$ conda create -n iTT -c conda-forge -c cmutel brightway2 jupyterlab
$ conda activate iTT
$ conda install --file requirements.txt
```

Close your terminal.

### Setup project

Change the `config.ini` file by giving to the `ECOINVENT` variable the path to the Ecospoild2 datasets of your Ecoinvent database.

Then, open your terminal, and type:

```
$ cd lcatools/isTerreTool
$ conda activate iTT
$ ./setup.py
```

Close your terminal.

### Use program

Now you should have everything in place.

Every time you want to use the program, you need to open your terminal.

Then go into the isTerreTool folder:

```
$ cd lcatools/isTerreTool
```

Acivate your conda environment:

```
$ conda activate iTT
```

Run the `server.py` script. It is recommended to use it with the `-t|--test` option:

```
$ ./server.py -t
```

## Install Activity-Browser

Activity-Browser is a graphical interface that can be useful to visualize grouped data. You can install it using `miniconda`:

```
$ conda activate iTT
$ conda install activity-browser
```

You have then juste to activate the `iTT` environment and type `activity-browser` in the terminal to open Activity-Browser.

# Full usage

The command-line program allows the user to choose some parameters. There are two way for the user to easily change the program behavior. One one hand he or she can modify the variables of the `config.ini` file. On the other hand, he or she can use command line arguments to change what the program will do.

## Key concepts

This program is based on [Brightway2](https://2.docs.brightway.dev/intro.html).

Brightway2 is a python framework used to work with LCA (Life Cycle Analysis) databases.

The setup script will create a new Brightway2 project, named as the `PROJECT_NAME` variable of the `config.ini` file. The default project name is `isterre`.

In this project, the script will import the Ecoinvent datasets given by the `ECOINVENT` variable of the `config.ini` file. That's why the first thing to do before using the setup script, is to setup in this variable with the path to your Ecospoild2 dataset.

The setup script will then create an empty database named by the variable `DATABASE_NAME` of the `config.ini` file. The default name of this database is `modified`. It is in this database that the grouped activities will be saved.

Once setup is complete, when you use the progam by running the `server.py` script, you will select the activities of the Ecoinvent database you wish to group, and the program will group them in a single activity, which will be saved in the new database created during the setup (the `modified` database).

If you want to visualize with a graphical interface the activities you previously grouped using the program, you can use the graphical interface for Brightway2: [Activity-Browser](https://github.com/LCA-ActivityBrowser/activity-browser).

See the Quick Start section to see how install activity-browser using `miniconda`. You can then run it by typing `activity-browser` in your terminal.

You can then select in the interface the `isterre` project, and double click on the `modified` database. You will see the list of the prevously grouped activities, and you can click on one of them to see details, use it to do an LCI, etc.

## Understanding CSV outputs

The options `-b|--biosphere`, `-f|--flows`, `-p|--production` of the `server.py` program generate CSV files in the `output` folder.

Thoses CSV files list all the biosphere or technosphere inputs of a grouped activity. Each input has its own metadata. Rows of the CSV files are inputs, and columns are the data about each of thoses inputs. Thoses files can be opened with Excel for instance, or any other program or script supporting CSV format.

The separaror is the "`;`" character.

You can choose which column of data you want to remove from the CSV export, by blacklisting them in the `EXCLUDE_BIOSPHERE` and `EXCLUDE_TECHNOSPHERE` variables of the `config.ini` file.

The biosphere flows CSV export have some extra columns, in addition to the ones that represent usual data. Thoses colunmns represent cumulative value of the same kind of flows. For instance, flows that have the same name, like `Carbon dioxide`, will be grouped and have the same cumulative value, calculated by adding the value of each flows having the same name.

**Thoses extra columns will never cumulate flows having a different `type`.** A filter will verify that, if we cumulate values of `Carbon dioxyde`, we will not cumulate values of carbon dioxyde relased in the environnement, and values of carbon dioxyde extracted from the environnement.

There are 6 of thoses extra columns :

* *sum by name:* Cumulative value of flows having the same value for the `name` field. For example, every `Carbon dioxyde` flows.
* *sum by name and category:* Cumulative value of flows having the same value for the `name` field and the same first element of the `categories` field. For example every `Carbon dioxyde` flows released in the `air`.
* *sum by name and subcategory:* Cumulative value of flows having the same value for the `name` field and the same value for the `categories` field. For example every `Carbon dioxyde` flows released in the `non-urban air or from high stacks`.
* *sum by subname:* Cumulative value of flows having the same value for the part of the `name` field preceding the first comma. For example, "`Zinc, ion`" will be considerated as the same as "`Zinc`".
* *sum by subname and category:* Cumulative value of flows having the same value for the part of the `name` field preceding the first comma, and the same first element of the `categories` field.
* *sum by subname and subcategory:* Cumulative value of flows having the same value for the part of the `name` field preceding the first comma, and the same value for the `categories` field.


## Command line arguments

### How to use

To use a command line argument, you need, when you run the program in your terminal, to indicate by a letter what you want the program to do, preceded by the symbol `-`.

*For instance:*

To run the program on Windows:

```
$ python server.py
```

To run it with the `-f|--flows` argument:

```
$ python server.py -f
```
or
```
$ python server.py --flows
```

To run the program on Linux:

```
$ ./server.py
```

To run it with the `-f|--flows` argument:

```
$ ./server.py -f
```
or
```
$ ./server.py --flows
```

You can also give multiple arguments at the same time. For instance to use the `-f|--flows` argument and the `-t|--test` argument at the same time, you can use one of the following (the order between arguments doesn't matter):

* `server.py -tf`
* `server.py -ft`
* `server.py -t -f`
* `server.py -f -t`
* `server.py --flows --test`
* `server.py --test --flows`

### Avilable arguments for the program

* `-b|--biosphere` : The command line interface proposed by using this option is quite different from the default one. Indeed, instead of grouping a production line, the user will select an already grouped activity in the database, and will export direct biosphere flows of the selected activity, in a CSV file. The folder in which this export will be made is the one named `output`. The file will have the same name as the group, with a `_bio.csv` extension.
  * `server.py -b`
  * `server.py --biosphere`
* `-t|--test` : Strongly recommended. At the end of the generation of grouped production line by the user, the program will test the data to be sure that it is reliable. In case that it is not, see the "What should I do if my test didn't pass?" section of this readme. Else, go ahead, the data is not corrupted.
  * `server.py -t`
  * `server.py --test`
* `-p|--production` : At the end of the generation of grouped production line by the user, the direct technosphere flows of this grouped activity will be exported in a CSV file. The folder in which this export will be made is the one named `output`. The file will have the same name as the group, with a `_tech.csv` extension.
  * `server.py -p`
  * `server.py --production`
* `-f|--flows` : At the end of the generation of grouped production line by the user, the direct biosphere flows of this grouped activity will be exported in a CSV file. The folder in which this export will be made is the one named `output`. The file will have the same name as the group, with a `_bio.csv` extension. This option is equivalent of standard grouping, followed by the use of the `-b|--biosphere` argument.
  * `server.py -f`
  * `server.py --flows`
* `-h|--help` : Shows all arguments available for the `server.py` program.
  * `server.py -h`
  * `server.py --help`


### Avilable arguments for the setup script

* `-f|--force` : By default, the `setup.py` script will detect if the project has already been setup. If so, it will not do it again. This argument forces the setup to erase previous setup, and setup the project again.
  * `setup.py -f`
  * `setup.py --force`
* `-h|--help` : Shows all arguments available for the `setup.py` script.
  * `setup.py -h`
  * `setup.py --help`

### Avilable arguments for the test script

* `-g|--generate` : Instead of running tests, this argment will run a command-line interface, allowing the user to manualy generate a new test file. This interface is very similar to the one by default of the `server.py` script. To use this argument, you need to give, after the `-g|--generate` argument, the name of the test you want to generate.
  * `test.py -g <test name>`
  * `test.py --generate <test name>`
* `-a|--auto` : Instead of running tests, this argment will run a command-line interface, allowing the user to automatically generate a given number of random tests. The interface will also ask for the maximum of activities each test can have in its group (you can for instance genearte lots of very little tests, or juste a few very huge tests).
  * `test.py -a`
  * `test.py --auto`
* `-k|--keep` : With this argument, the script will run tests, but will keep all the generated activities during testing in the project's database. This is useful for instance to identify which test is problematic and why, by using for instance Brightway2's interface: [Activity-Browser](https://github.com/LCA-ActivityBrowser/activity-browser).
  * `test.py -k`
  * `test.py --keep`
* `-l|--loop` : Depreciated. Allow to test multiple time the same test, to see the difference of result from one time to another. This option is not very useful anymore, because every test that doesn't pass the checking, go throught a Monte Calro verification, which acts quite like this option, but atomatically. To use this option, you need to give the path to the test file after the `-l|--loop` argument. The number of iteration can be configurd with the `ITERATIONS` variable in the `config.ini` file.
  * `test.py -l <test file path>`
  * `test.py --loop <test file path>`
* `-h|--help` : Shows all arguments available for the `test.py` script.
  * `test.py -h`
  * `test.py --help`

## Configuration file

Lots of variables can be modified in the `config.ini` file, in the root folder.

The description of the variables of this file are given by the comment preceding the variable, inside the file.

The most important variable is the `ECOINVENT` one: any user who wants to use this program have to set to this variable the path to its Ecoinvent Ecospoild2 dataset. He or she can then run the `setup.py` script.

There are 5 types of configurable variables:

* The `SETUP` ones: variables for which any modification involves to reboot the whole application by running the `setup.py` script (potentially with `-f|--force` argument).
* The `TEST` ones: variables that are used in the test system. Thoses are separated in two categories: verification thresholds of the test system, and other test-related variables.
* The `LOG` ones: variables that configure elements for logged tests' informations. When a test is done, a set of data is saved in a specific directory, to do manual verification if a problem occures.
* The `RUNTIME` ones: variable concerning python's runtime logger.
* The `API` ones: variables to configure data in exported CSV files. Mostly lists of CSV columns to ignore or to sort the CSV file by.

# Development

This section is written for developers who wants to understand the program's code, to take over the futur of this tool.

## Technical documentation

The functions and class Docstrings are based on [Google python style](https://google.github.io/styleguide/pyguide.html), with some basic markdown for specific layout.

The documentation can so be compiled using a documentation generator. 
The one that has been tested is [pdoc3](https://pdoc3.github.io/pdoc/).

For example for linux user with firefox:

```
$ pip3 install pdoc3
$ cd <isTerreTool location>
$ pdoc --html . -o doc
$ firefox doc/isTerreTool/index.html
```

## Code architecture

### Brightway2

The main framework of this program is [Brightway2](https://brightway.dev/).

To understand the way that Brightway2 is used in this program, please start to read the beginning of [its documentation](https://2.docs.brightway.dev/intro.html) (at least until you understand the main components of Brightway2 structure : projects, databases, activities, exchanges, ...). Then, please read the "Key concepts" section of this readme.

Why using Brightway2? Because it is the reference library to work with LCA in an unusual way. It is also open-source, which is very useful for modifing some of its functioning, and to maintain it over time with the community.

*Brightway2 is used in this program as follows:*

The setup create the project, import Ecoinvent database (`ecoinvent`), and create an empty database (`modified`).

The user then seeks in the activities of the `ecoinvent` database the terminal activity of the goupment he or she whishes to do. He/she then trace back the production line, as far as desired, by selecting one by one activities from the `ecoinvent` database that are linked to the previously selected ones. **The code structure does not allow to build a discontinous production line**, `RuntimeError` will be raised if something like that is attempted.

The program will then group thoses activities in a single grouped activity, which will be saved in the `modified` database. It can exports inputs of this grouped activity in CSV files, and/or tests the grouped activity to be sure that data is reliable.

### Versions

The dependancies of the program are given by the `requirements.txt` file. It is recommanded to install them with `miniconda` in a dedicated `conda` environment.

If dependancies errors appear, the program has been developed with the following versions of packages:

```
brightway2==2.3
colorama==0.4.5
numpy==1.23.1
matplotlib==3.3.4
scipy==1.8.1
termcolor==1.1.0
pandas==1.4.3
progressbar2==4.0.0
networkx==2.8.4
pygraphviz==1.9
pillow==9.2.0
```

You can then try to downgrade the problematic package.

**This program uses functionnalities of `python 3.8`, you need at least use this version or an higher one**. The `python` version with which the developement has been made is the `3.8.5`.

### Model-View-Controller

The global development follows an [MVC architecture](https://en.wikipedia.org/wiki/Model%E2%80%93view%E2%80%93controller). This can be powerful to easly add fuctionalities or change the interface.

#### Model

The model is defined by a single class developed in `src.model.group` called `Activity_group`.

This class represents a set of activities to group (EG a production line). You can with it add activities, define metadata, compile the group into a single activity, and test this grouped activity.

#### View

The current view is a simple command-line interface, that can be run by calling `src.interface.command_line.command_line_interface`. 

The goal is, in the futur, to use a `Flask` app to create an online interface, that can be deployed with `Docker`.

#### Controller

The controller is the connection between the view and the model.

It is composed of a single class developed in `src.controller.controller`, called `Controller`.

This class in a [singleton](https://en.wikipedia.org/wiki/Singleton_pattern). This means that every instance of the class will be a reference to an unique element. **This singleton constraint is surely destined to disappear, if the program has to be deployed in a real-time online interface, with concurrent access.**

An example of the use of this controller is given in the class's docstring.

**A devlopment of interface should never call the `src.model.group.Activity_group` directly, but rather use `src.controller.controller.Controller`.**

## Groupment algorithm

![Group of activities](https://gitlab.inria.fr/steep/lcatools/uploads/f1915c1fcf435458d77d517015d86613/multiple.png)

### What does the program do?

The program take a database of activities: Ecoinvent in real case. Yet, for the example, we will demonstrate the algorithm a database made of 9 activities, as showed in the picture.

It ask the user to select a subgroup of thoses linked activities: the one surrounded by dotted lines in the picture of instance.

It then group them in a single grouped activity: the `Ag` one in the example.

### The main goal of the program

All the challenge in this case is, form the values $`p_i`$, to find the values $`p^g_i`$.

### How do we do?

We use input-output model, with [Leontief inverse matrix](https://en.wikipedia.org/wiki/Input%E2%80%93output_model).

We first create the matrix of the dependancies between the activities inside the group:

![A matrix](https://gitlab.inria.fr/steep/lcatools/uploads/7a9e68f8dfd107ae7099757bb6e9b649/A_matrix_mini.png)

If some of the values of this matrix are negative, we take their absolute values. That can happen in `Ecoinvent` for treatement process. Yet, with this method, we need positive values to obtain a valid result.

Then, we calculate the Leontief inverse matrix of this matrix:

![L matrix](https://gitlab.inria.fr/steep/lcatools/uploads/d6f2680bb63ed5261f5f1366ea4003cc/L_matrix_mini.png)

We reduce this matrix to the column corresponding to the terminal activity:

![AL1 vector](https://gitlab.inria.fr/steep/lcatools/uploads/343cf1af2745ff8daaeb2c59f21dca04/A1L_vector_mini.png)

We create the matrix corresponding to the flows that come from the outside to the inside of the group:

![F matrix](https://gitlab.inria.fr/steep/lcatools/uploads/207cc161803ae84a6a6b42fd58bfa3b6/F_matrix_mini.png)

We then multipliate thoses two last matrix to obtain a vector corresponding the expected inputs of the grouped activity.

![Pg vector](https://gitlab.inria.fr/steep/lcatools/uploads/89385b44ac4e714b0868a543984e4be7/Pg_vector_mini.png)

## Docker

Initial Dockerfile found here: https://github.com/cmutel/bw2-docker

**The current Dockerfile is not functional, and have not even been tested. This is for futur relases.**

## What's next?

Here are some ideas of stuff to do in the futur:

* Create a web interface, with [`Flask`](https://flask.palletsprojects.com/en/2.1.x/) for instance.
* Create a full API, which can be also done with `Flask`.
* Develop the deployment system, can be done with [`Docker`](https://www.docker.com/). The current Dockerfile is not reliable.
* Stabilise matrices with a [preconditioner](https://en.wikipedia.org/wiki/Preconditioner). The matrices to stabilize can be the ones of the groupment algorithm, in the `src.model.group.Activity_group._build_leon_extern_matrix` method, or the ones of `Brightway2` LCA matrices in the `bw2calc.lca.LCA` class. Some functions in this project has been developed in this purpose, but are not currently used (because not functional yet), like in the `src.utils.tests.LCA` file.
* Develop a direct interface with MATER's code, using [`pandas`](https://pandas.pydata.org/).
* Add a functionality of semi-automatic grouping, for similar activities that come from different localization. Many times, the production line of thoses activities are similar except for localization, so the system, following the groupment of one localization, could atomatically determine the production line for the others localizations.
* Add the possibility to identify which causal line of the grouped activity's LCA have the more final impact, to see which part of thoses materials must be added in priority in MATER, if they are not yet included in the model. For example, currently MATER does not take into account wood material. However, it should if the environmental impact is not negligible.
* Predict data of the futur by estimating performances of futur industries, using [WURST](https://wurst.readthedocs.io/), to integrate this futurist data into MATER model.
* Allowing the program to import others versions of databases. The Brightway2 comnunity is, for instance, currently trying to implement zolca support.

# Tests

## Test system presentation

![Testing system infographic](https://gitlab.inria.fr/steep/lcatools/uploads/d80ddaa7457da2cb64d715ff95fc27fc/test_system_en.png)

### Global idea

The idea of the test system, is to compare the LCI of the grouped activity to the LCI of the terminal activity of the group. They should be the same.

### Vectors to compare

Every LCI gives a vector of biosphere flows amount. We have then two vectors that should be identical term to term:

* $`g^{grp}`$: from the LCI of the grouped activity
* $`g^{act}`$: from the LCI of the terminal activity

### Problem

This method works in theory but not quite in practice.

Indeed, the algebra of LCI calculus is numerically unstable, beacause of bad condition number of technosphere matrix, which implies numerical errors due to rouning of floats.

The problem is that thoses numerical errors are not the same on both $`g^{grp}`$ and $`g^{act}`$. So even if the test should pass, thoses vectors will not have the exact same values.

We then have to differentiate errors that are "usual", and the ones that show a bug in the code, and which invalidate the output of the algorithm (eg. the inputs of the grouped activity).

### Statistics vectors

To differentiate numerical errors, we need to consider divergence thresholds beyond which the difference between the tho vectors are considered to be abnormal.

First, we need to create two new vectors: the one that contains the abolute error of $`g^{grp}`$ and $`g^{act}`$ term to term: $`g^{abs}`$, and the one that contains the relative error of $`g^{grp}`$ and $`g^{act}`$ term to term: $`g^{rel}`$.

* $`g^{abs} = abs(g^{act} - g^{grp})`$
* $`g^{rel} = \frac{abs(g^{act} - g^{grp})}{abs(g^{act})}`$

Then we do statistics on thoses two vectors ($`g^{abs}`$ and $`g^{rel}`$), and others statistics, like their maximum, their minimum, ...

Finally, we choose thresholds on thoses statistics, and we verify that the statistics respect thoses thresholds. If yes, the test passes.

### Monte Carlo verification

![Monte Carlo infographic](https://gitlab.inria.fr/steep/lcatools/uploads/81760c089c16a46435af5677a61e797a/monte_carlo_en.png)

One last verification is done if the test didn't pass.

This is kind of a Monte Carlo verification: even if it is not technically one, because Monte Carlo verification choose random values on a set of parameters, where the current method just changes one parameter by incrementing it by one. However, the result looks random, and is averaged out to approach a constant value.

If the test didn't pass, that means that one (or more) statistic didn't respect the corresponding threshold.

Yet, this can be an exceptional situation: bad luck making a float rouding at the bad moment, that ended up to a ponctual significant error.

To be sure that this error is only exceptional, we do this last Monte Carlo verification. We redo the test many times, keep in mind the value of the problematic statistic every time, and at the end, we average thoses values.

We then check if the averaged value respect the threshold. If yes, this was a bad alert and the test passes. If not, there is probably a problem.

## Test system sequence diagram

### Bitmap image

![Sequence diagram](https://gitlab.inria.fr/steep/lcatools/uploads/d1f830d3c4a0b316c08511e853bf8a69/tests_sequence_diagram.png)

### Mermaid style

```mermaid
sequenceDiagram
    participant Interface
    participant Controller
    participant Activity_group
    participant Checker
    Interface->>Controller: save_and_test
    Controller->>Activity_group: compile_save_and_test
    Activity_group-->>Controller: absolute, relative, others
    Controller->>Checker: check_level
    Checker-->>Controller: warnings, errors
    Controller-->>Interface: code, elements
    opt code > 1
      Interface->>Controller: verify_errors_stability
      Controller->>Activity_group: monte_carlo_test
      Activity_group->>Checker: Checker.list
      Checker-->>Activity_group: mean, func
      Activity_group-->>Controller: Boolean
      Controller-->>Interface: Boolean
    end    
```

## Test system parameters

*To get informations about flags of the script's options, please see "Avilable arguments for the test script" section of this readme.*

Some test system parameters are are configurable in the `config.ini` file. Thoses are separated in two cartegories:

* The checker's functions parameters: thoses are the thresholds on statistics on vectors of comparaison: the absolute errors vector, the relative errors vector, and some others statistics.
  
| Variable | Linked statistic | Warning/Error | Found value needs to be... | Current value | Monte Carlo mean |
| ----------- | ----------- | ----------- | ----------- | ----------- | ----------- |
| `RELATIVE_TOLERANCE_FACTOR` | $`g^{rel}`$ Maximum | Warning | Below | 0.001 | Geometric |
| `RELATIVE_EXTREME_TOLERANCE_FACTOR` | $`g^{rel}`$ Maximum | Error | Below | 0.1 | Geometric |
| `ABSOLUTE_TOLERANCE_FACTOR` | $`g^{abs}`$ Maximum | Warning | Below | 0.1 | Geometric |
| `ABSOLUTE_EXTREME_TOLERANCE_FACTOR` | $`g^{abs}`$ Maximum | Error | Below | 100 | Geometric |
| `RELATIVE_BENFORD_TOLERANCE_FACTOR` | $`g^{rel}`$ p value of Benford's chi-squared test | Warning | Above | 0.1 | Arithmetic |
| `ABSOLUTE_BENFORD_TOLERANCE_FACTOR` | $`g^{abs}`$ p value of Benford's chi-squared test | Warning | Above | 0.9999 | Arithmetic |
| `ABSOLUTE_MIN_TOLERANCE_FACTOR` | $`g^{abs}`$ Minimum | Error | Above | 0 | Geometric |
| `SINGULARITY_TOLERANCE_FACTOR` | Singularities: number of times a value is null in one of the initial vectors and not null in the other | Error | Below | 0.9 | Arithmetic |

* Other parameters: number of iterations for Monte Carlo, database used to test, ...
  * `VALID_RELATIVE_ERROR_CUTOFF`: The values to compare of $`g^{grp}`$ and $`g^{act}`$ that are below this value will be ignored (considered as zeros).
  * `TEST_DIR`: Name of directory containing test files.
  * `TEST_AUTO_DIR` Name of subdirectory containing automatically generated test files.
  * `DATABASE_TEST`: Name of the database used to execute tests.
  * `MONTE_CARLO`: Number of times the Monte Carlo verification must iterate.
  * `ITERATIONS`: Number of times the `-l|--loop` option of the `test.py` script will test the given file.

See comments of `config.ini` file for additional informations about each variable.

## Focus on Checker

One key class is at the center of the test system: the `src.utils.tests.checker.Checker` class.

The goal of this class is to contain the audit system of the statistics:

* Containing informations about the statistics, the corresponding thresholds, and the corresponding Monte Carlo mean (geometric or arithmetic, depending on the assiociated statistic).
* Provide functions that take as argument a statistic, and give in return a value saying if the statistic is valid, if a warning must be raised or if an error is detected.
  
This class have as attribute three dictionaries, correspondig to the 3 types of statistics: the ones on the absolute errors vector, the ones on the relative errors vector, and the others.

Thoses dictionaries have got as keys a subset of the keys from the corresponding dictionaries of statistics. This subset corresponds to the statistics we want to test (all statistics are not tested).

The value associated with a given key of thoses dictionaries is a 2D-tuple.

This 2D-tuple contains functions. The first function is the audit function. The second is the mean function.

### Audit function

The audit function of a given statistic is a function that take the statistic as parameter, and returns a code:

* 0 if the statistic has an acceptable value.
* 1 if a warning must be raised about the statistic's value (gray zone).
* 2 if the statistic does not have an acceptable value.

Thoses functions use the `config.ini` test's variables to choose which value to return.

### Mean function

The Monte Carlo verification needs to do a mean of a tested statistic. Yet, the classic arithmetic mean is not always relevant because the orders of magnitude can be very different from one time to another. Sometimes, this is the order of magnitude that we want to average, not the value. In thoses cases, grometric mean is more relevant.

## What should I do if my test didn't pass?

Well, you can dig a little more to try to understand why the test didn't pass. For instance:

* Additional informations can been found in `./output/logs/<name of group>` (more specifically the directory path given by the `LOG_DIRECTORY` variable of the `config.ini` file), especially an image named `found_function_of_ref`. This graph plots the $`g^{grp}`$ values in function of $`g^{act}`$ values. This should theoretically follows the reference $`x=y`$ linear curve. You can quickly see with this plot if huge differences occured. Be careful, this plot is in logarithmic scale on both axes.
* Check which statistic didn't pass the test: this should be writen in your terminal interface just before the Monte Carlo verification. If the problematic statistic is the `Singularity` one, then don't take too much credit for test result, this statistic has not been sufficiently studied to be sure that the corresponding threshold is significant. On the other hand, the more studied statistic is the maximum of the relative errors vector. This one should be quite reliable, even if you have to keep in mind that the testing system isn't perfect.
* Check if a warning saying "`Unbalanced group, condition number of intern matrix equals to <number>. Might create some errors in result...`" has been raised, this can explain some unstabilities in result.

So, here what I recommand you to do if the problem occures:

### If you are just a user, not a developer

Your best guess is to check the `found_function_of_ref` plot, and check if it follows well the $`x=y`$ line (only one or two deviations can be considered negligible, depending on your judgment, I have no fixed idea about this question myself). 

* If yes, there are two possibilities:
  * The group you made is simple: linear (no loop), no negative links, juste a fews activities, then you might trust data because the program is quite reliable on thoses cases (according to your reliability requirements, but if they are very hight then, first you should not use this program, which is not stable enough, and second you should probably not even use LCA data).
  * The group you made is complex, then you will have to decide for yourself, because opposing interpretations can be made. On one hand, the data might not be reliable due to an unforeseen situation that caused a program bug, so you could need a developer to fix it, and you should not use the generated data. On the other hand, the errors can also be due to a fundamental instability from the structure of the group, and there's not much we can do about it yet, so you might use generated data, for lack of better. The warning mentioned before can be a clue in the direction of this second interpretation.
* If not, this can be due to dependancie problems between python pakages, especially if the errors happens often. Yet, you might try to install requirements again, with the versions given in the "Versions" section of this readme. If this doesn't solve the problem, you migh need a devloper to see if there is a bug to fix in the program, or to make a rebalancing of verification threasholds.

### If you are a developer

First it can be instersting to look at the instructions given to the non-developers in this situation.

Then you can ether:

* Try to debug a potential bug, probably due to a specific configuration of the group. See the groupment algorithm, and look at the `src.model.group.Activity_group` class, you will probably have to debug something in it. The graphical interface Activity-Browser can be of great help.
* Try to get a better understanding of mechanisms behind the deviations of the different statistics, and reajust thresholds according to your findings.
* Try to stabilize matrix inversion with a preconditioner. This can be applied to the internal matrix of groups (see the groupment algorithm), but also to `Brightway2` LCI technosphere and biosphere matrices.
