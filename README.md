# lcatools

**Name**: LCA tools

**Author**: Baptiste de GOËR (Grenoble INP - Ensimag student, last year)

**Year**: 2022

## Presentation

Set of tools to work with LCA in STEEP team and Mater project.

This set has been developed by Baptiste de GOËR during his internship at the STEEP Team, from Febuary to July 2022.

## Tools

| Tool | Goal | Folder | Dev branch |
| -------------------- | --------- | ------ | ------ |
| Classification maker | Append classification system to Ecospoild2 `Classifications.xml` file | classificationsMaker | categoriesTool |
| Mater groupment tool | Interface to group activities and use thoses in Mater model | isTerreTool | isTerre |
